# opc-service

Public copy of some of the OPC service code.

The 'src' directory contains the code used in the Simplified Provisioner, which includes code that interacts with Openstack and Ansible Tower. Initially, we started to use Openstack directly for most of the workflow but then switched over to using Ansible Tower, so most of the the heavy lifting is handle via Ansible Tower (server creation/deletion, assigning a floating IP to a new server, etc.). The Openstack code does, however, still contain code for creating/deleting servers, etc...

The 'os' directory contains API code that interacts only with Openstack. It is a re-write and is a bit cleaner and more flushed out, though not complete (i.e. does not contain code to assign a floating IP to a server).
