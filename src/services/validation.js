/**
 * validation module
 * @module validation
 */
/**
 * Copyright 2018 Virginia Polytechnic Institute and State University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// global(s)

const USER_AUTH_SVC = 'Auth Service';
const ENTERPRISE_DIR_SVC = 'Enterprise Directory Service';

const USERS_AND_GROUPS_DELIMITER = ',';

const config = {
  userAuthSvc: '',
  enterpriseDirSvc: ''
};

// required module(s)

const fs = require('fs');
const httpStatus = require('http-status-codes');
const jwt = require('jsonwebtoken');
const logger = require('simlog').getLogger();
const simpleRp = require('simple-request-promise');

// required local module(s)

const HttpError = require('../utils/HttpError');

/**
 * @function generateJwt
 * @description Generates a JWT for quering the account and ed services.
 */
function generateJwt() {
  const token = jwt.sign({}, config.jwtCert, {
    algorithm: 'RS256',
    issuer: 'orca-web',
    subject: 'ORCA_SERVICE:opc-api-service'
  });

  return token;
}

/**
 * @function health
 * @description Check that we can query the accounts and enterprise
 *   directory services.
 * @param {object} healthResult check health result
 * @return none
 */
async function health(healthResult) {
  logger.debug();

  try {
    const token = generateJwt();

    const rp = simpleRp.getInstance();
    rp.header('Authorization', `Bearer ${token}`);

    rp.options.uri = config.userAuthSvc.healthUrl;
    let response = await rp.get();
    if (response.statusCode === httpStatus.OK) {
      healthResult.resultUp(USER_AUTH_SVC);
      logger.debug(USER_AUTH_SVC, 'is UP');
    } else {
      healthResult.resultDown(USER_AUTH_SVC);
      logger.debug(USER_AUTH_SVC, 'is DOWN');
    }

    rp.options.uri = config.enterpriseDirSvc.healthUrl;
    response = await rp.get();
    if (response.statusCode === httpStatus.OK) {
      healthResult.resultUp(ENTERPRISE_DIR_SVC);
      logger.debug(ENTERPRISE_DIR_SVC, 'is UP');
    } else {
      healthResult.resultDown(ENTERPRISE_DIR_SVC);
      logger.debug(ENTERPRISE_DIR_SVC, 'is DOWN');
    }
  } catch (error) {
    logger.error(error.message);
    healthResult.resultUnknown(USER_AUTH_SVC);
    healthResult.resultUnknown(ENTERPRISE_DIR_SVC);
  }
}

/**
 * @function validateGroups
 * @description Validates the given array of LDAP group(s). It is assumed
 *   that there are not empty (blank) groups.
 * @param {string} token JWT to use for the request(s)
 * @param {array} groups LDAP group(s) to validate
 * @returns {array} invalid LDAP group(s)
 * @private
 */
async function validateGroups(token, groups) {
  logger.debug('{groups: %s}', groups);

  const invalidLdapGroups = [];

  if (groups && groups.length !== 0) {
    // setup request
    const rp = simpleRp.getInstance('');
    rp.header('Authorization', `Bearer ${token}`);

    // request group info
    const results = [];
    for (let i = 0; i < groups.length; i += 1) {
      rp.options.uri = config.enterpriseDirSvc.groupUrl.replace(':group', groups[i]);
      results.push(rp.get());
    }

    // wait for all requests to complete
    const responses = await Promise.all(results);

    // process results
    for (let i = 0; i < responses.length; i += 1) {
      if (responses[i].statusCode !== httpStatus.OK) {
        throw new HttpError(responses[i].statusCode, responses[i].body);
      } else if (responses[i].body.length === 0) {
        invalidLdapGroups.push(groups[i]);
      }
    }
  }

  logger.trace('{groupsNotFound: %s}', invalidLdapGroups);

  return invalidLdapGroups;
}

/**
 * @function validateUsers
 * @description Validates the given array of user(s). It is assumed
 *   that there are no empty (blank) users.
 * @param {string} token JWT to use for the request(s)
 * @param {array} users user(s) to validate
 * @returns {array} invalid user(s)
 * @private
 */
async function validateUsers(token, users) {
  logger.debug('{users: %s}', users);

  const invalidUsers = [];

  if (users && users.length !== 0) {
    // setup request
    const rp = simpleRp.getInstance('');
    rp.header('Authorization', `Bearer ${token}`);

    // request user info
    const results = [];
    for (let i = 0; i < users.length; i += 1) {
      rp.options.uri = config.userAuthSvc.userUrl.replace(':user', users[i]);
      results.push(rp.get());
    }

    // wait for all requests to complete
    const responses = await Promise.all(results);

    // process results
    for (let i = 0; i < responses.length; i += 1) {
      if (responses[i].statusCode !== httpStatus.OK) {
        invalidUsers.push(users[i]);
      }
    }
  }

  logger.trace('{usersNotFound: %s}', invalidUsers);

  return invalidUsers;
}

/**
 * @function validateUsersAndGroups
 * @description Validates the given list of users/LDAP groups.
 * @param {string} usersAndGroups list of user(s) and/or LDAP group(s)
 * @returns {string} list of invalid user(s) and LDAP group(s)
 * @public
 */
async function validateUsersAndGroups(usersAndGroups) {
  logger.debug('{usersAndGroups: %s}', usersAndGroups);

  // remove empty values and trim
  const normalizedUsersAndGroups = [];
  const data = usersAndGroups.split(USERS_AND_GROUPS_DELIMITER);
  for (let i = 0; i < data.length; i += 1) {
    const entity = data[i].trim();
    if (entity) {
      normalizedUsersAndGroups.push(entity);
    }
  }

  // create JWT to use
  const token = generateJwt();

  // validate groups; anything returned is either
  //   1) an invalid group or 2) a user
  const invalidGroups = await validateGroups(token, normalizedUsersAndGroups);

  // validate users; anything returned is neither a user nor a group
  const invalidUsersAndGroups = await validateUsers(token, invalidGroups);

  logger.trace(
    '{usersAndGroups: %s, notFound: %s}',
    normalizedUsersAndGroups,
    invalidUsersAndGroups
  );

  return {
    usersAndGroups: normalizedUsersAndGroups.join(USERS_AND_GROUPS_DELIMITER),
    notFound: invalidUsersAndGroups.join(USERS_AND_GROUPS_DELIMITER)
  };
}

/**
 * @function initialize
 * @description Initialize the module.
 * @param {object} valConfig validation configuration
 * @public
 */
function initialize(valConfig) {
  logger.stringifyObjects(true);
  logger.debug('%o', valConfig);

  // URL used for user validation
  config.userAuthSvc = {
    healthUrl: `${valConfig.userAuthSvcUrl}/health`,
    userUrl: `${valConfig.userAuthSvcUrl}/ed/username/:user`
  };

  // URL used for group validation
  config.enterpriseDirSvc = {
    healthUrl: `${valConfig.enterpriseDirSvcUrl}/health`,
    groupUrl: `${valConfig.enterpriseDirSvcUrl}/edgroup/groups/:group`
  };

  // certificate for JWT creation
  config.jwtCert = fs.readFileSync(valConfig.certFile);
}

/**
 * module export(s)
 */

module.exports = {
  health,
  initialize,
  validateUsersAndGroups
};
