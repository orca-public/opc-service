/**
 * OpenStack Image API
 * @module openStackImage
 */
/**
 * Copyright 2018 Virginia Polytechnic Institute and State University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * global(s) and required module(s)
 */

const httpStatus = require('http-status-codes');
const logger = require('simlog').getLogger();
const simpleRp = require('simple-request-promise');

const HttpError = require('../utils/HttpError');

// OpenStack config; shared among OpenStack modules but only updated by
//   the parent module
let apiConfig = {};

/**
 * @function getImage
 * @description Returns image details for the given image ID.
 * @param  {string} imageId ID of image to retrieve details for
 * @returns {object} image details
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getImage(imageId) {
  logger.debug('(imageId: %s)', imageId);

  const uri = apiConfig.uri.image.getImage.replace(':imageId', imageId);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token);

  // fetch image information
  const response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'error retrieving image details', response.body);
  }

  logger.trace(response.body);
  return response.body;
}

/**
 * @function getImages
 * @description Returns images.
 * @param {string} imageStatus (optional) return images with the
 *   given status; by default, 'active' images are returned; values:
 *   active,deactivated,deleted,importing,killed,pending_delete,
 *   queued,saving,uploaded
 * @returns {array} available images
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getImages(imageStatus) {
  const status = imageStatus || 'active';
  logger.debug('(status: %s)', status);

  const uri = apiConfig.uri.image.getImages.replace(':status', status);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token);

  // fetch images
  const response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'error retrieving images', response.body);
  }

  logger.trace(response.body.images);
  return response.body.images;
}

/**
 * @function initialize
 * @description Initializes the module. Functions are not directly
 *   exported. They are added to the export for the parent module.
 * @param  {object} config OpenStack config
 * @return none
 * @public
 */
function initialize(config) {
  logger.stringifyObjects(true);
  logger.debug();

  // OpenStack config (uri's, paths, etc...)
  apiConfig = config;
}

/**
 * module export(s)
 */

module.exports = {
  getImage,
  getImages,
  initialize
};
