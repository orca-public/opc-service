/**
 * OpenStack Network API
 * @module openStackNetwork
 */
/**
 * Copyright 2018 Virginia Polytechnic Institute and State University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * global(s) and required module(s)
 */

const httpStatus = require('http-status-codes');
const logger = require('simlog').getLogger();
const simpleRp = require('simple-request-promise');

const HttpError = require('../utils/HttpError');

// OpenStack config; shared among OpenStack modules but only updated by
//   the parent module
let apiConfig = {};

/**
 * @function getNetwork
 * @description Returns the details for the given network.
 * @param  {string} networkName name of network to query
 * @returns {object} network details
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getNetwork(networkName) {
  logger.debug('(networkName: %s)', networkName);

  const uri = apiConfig.uri.network.getNetwork.replace(':networkName', networkName);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token);

  // get network details
  const response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'error retrieving network details', response.body);
  }

  logger.trace(response.body);
  return response.body;
}

/**
 * @function initialize
 * @description Initializes the module. Functions are not directly
 *   exported. They are added to the export for the parent module.
 * @param  {object} config OpenStack config
 * @return none
 * @public
 */
function initialize(config) {
  logger.stringifyObjects(true);
  logger.debug();

  // OpenStack config (uri's, paths, etc...)
  apiConfig = config;
}

/**
 * module export(s)
 */

module.exports = {
  getNetwork,
  initialize
};
