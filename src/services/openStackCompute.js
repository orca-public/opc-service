/**
 * OpenStack Compute API
 * @module openStackCompute
 */
/**
 * Copyright 2018 Virginia Polytechnic Institute and State University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * global(s) and required module(s)
 */

const httpStatus = require('http-status-codes');
const logger = require('simlog').getLogger();
const simpleRp = require('simple-request-promise');

const HttpError = require('../utils/HttpError');

// OpenStack config; shared among OpenStack modules but only updated by
//   the parent module
let apiConfig = {};

/**
 * @function createServer
 * @description Create a server instance.
 * @param  {object} args needed arguments object
 *   {     serverName: <name to associate with new server>,
 *            imageId: <ID of image to create instance from>,
 *           flavorId: <instance flavor (RAM size, disk size, etc.)>,
 *          networkId: <ID of network instance will connect to>,
 *        keyPairName: <SSH keypair to allow access (optional)>,
 *           metadata: <object with key/value pairs (optional)>,
 *     securityGroups: <array of security access groups (optional)> }
 * @returns {string} ID of the newly created server instance
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function createServer(args) {
  logger.debug('(%o)', args);

  const uri = apiConfig.uri.compute.createServer;
  logger.trace('uri: %s', uri);

  const body = {
    server: {
      imageRef: args.imageId,
      flavorRef: args.flavorId,
      key_name: args.keyPairName,
      metadata: args.metadata,
      name: args.serverName,
      networks: [{ uuid: args.networkId }],
      security_groups: []
    }
  };

  // add security groups
  args.securityGroups.forEach(name => {
    body.server.security_groups.push({ name });
  });

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token).body(body);

  // create instance
  const response = await rp.post();
  if (response.statusCode !== httpStatus.ACCEPTED) {
    throw new HttpError(response.statusCode, 'error creating server', response.body);
  }

  logger.trace(response.body.server);
  return response.body.server.id;
}

/**
 * @function deleteServer
 * @description Deletes the server with the given ID.
 * @param {string} serverId ID of server to delete
 * @returns {string} ID of server that was deleted
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function deleteServer(serverId) {
  logger.debug('(serverId: %s)', serverId);

  const uri = apiConfig.uri.compute.deleteServer.replace(':serverId', serverId);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token);

  // delete the server
  const response = await rp.delete();
  if (response.statusCode !== httpStatus.NO_CONTENT) {
    throw new HttpError(response.statusCode, 'error deleting server', response.body);
  }

  return serverId;
}

/**
 * @function getFlavor
 * @description Return flavor information for the given flavor ID.
 * @param  {number} flavorId ID of flavor to retrieve info for
 * @returns {object} flavor information
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getFlavor(flavorId) {
  logger.debug('(flavorId: %d)', flavorId);

  const uri = apiConfig.uri.compute.getFlavor.replace(':flavorId', flavorId);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token);

  // fetch flavors
  const response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(
      response.statusCode,
      'error retrieving flavor details',
      response.statusCode
    );
  }

  logger.trace(response.body.flavor);
  return response.body.flavor;
}

/**
 * @function getFlavors
 * @description Returns an array available flavors.
 * @returns {array} projects user has access to
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getFlavors() {
  logger.debug();

  const uri = apiConfig.uri.compute.getFlavors;
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token);

  // fetch flavors
  const response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'error retrieving flavors', response.body);
  }

  logger.trace(response.body.flavors);
  return response.body.flavors;
}

/**
 * @function getMetadata
 * @description Returns the metadata for the given server.
 * @param {string} serverId ID of server to retrieve metadata for
 * @returns {object} server metadata if found, null if not found
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getMetadata(serverId) {
  logger.debug('(serverId: %s)', serverId);

  const uri = apiConfig.uri.compute.getMetadata.replace(':serverId', serverId);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token);

  // retrieve metadata
  const response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'error retrieving metadata', response.body);
  }

  logger.trace(response.body.metadata);
  return response.body.metadata;
}

/**
 * @function getServer
 * @description Return information for a specific server.
 * @param {string} serverId ID of server to retrieve details for
 * @returns {array} server information if found, null if not found
 * @throws {HttpError} if error occurs
 * @public
 */
async function getServer(serverId) {
  logger.debug('(serverId: %s)', serverId);

  const uri = apiConfig.uri.compute.getServer.replace(':serverId', serverId);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token);

  // fetch the server information
  const response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'error retrieving server details', response.body);
  }

  logger.trace(response.body.server);
  return response.body.server;
}

/**
 * @function getServers
 * @description Returns an array of servers.
 * @param  {string} owner (optional) return servers for this owner
 *   (metadata: vt_owner)
 * @returns {array} servers for the given owner; all servers if
 *   owner not given
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getServers() {
  logger.debug();

  const uri = apiConfig.uri.compute.getServers;
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token);

  // fetch servers
  const response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'error retrieving servers', response.body);
  }

  logger.trace(response.body.servers);
  return response.body.servers;
}

/**
 * @function rebootServer
 * @description Reboot the server with the given ID.
 * @param {string} serverId ID of server to reboot
 * @param {string} type reboot type (soft, hard)
 * @returns none
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function rebootServer(serverId, rebootType) {
  const type = rebootType ? rebootType.toUpperCase() : 'SOFT';
  logger.debug('(serverId: %s, type: %s)', serverId, type);

  const uri = apiConfig.uri.compute.serverAction.replace(':serverId', serverId);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token).body({ reboot: { type } });

  // reboot server
  const response = await rp.post();
  if (response.statusCode !== httpStatus.ACCEPTED) {
    throw new HttpError(response.statusCode, 'error rebooting server', response.body);
  }

  return serverId;
}

/**
 * @function startServer
 * @description Starts the server with the given ID.
 * @param {string} serverId ID of server to start
 * @returns none
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function startServer(serverId) {
  logger.debug('(serverId: %s)', serverId);

  const uri = apiConfig.uri.compute.serverAction.replace(':serverId', serverId);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token).body({ 'os-start': null });

  // stop the server
  const response = await rp.post();
  if (response.statusCode !== httpStatus.ACCEPTED) {
    throw new HttpError(response.statusCode, 'error starting server', response.body);
  }

  return serverId;
}

/**
 * @function stopServer
 * @description Stops the server with the given ID.
 * @param {string} serverId ID of server to start
 * @returns none
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function stopServer(serverId) {
  logger.debug('(serverId: %s)', serverId);

  const uri = apiConfig.uri.compute.serverAction.replace(':serverId', serverId);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token).body({ 'os-stop': null });

  // stop the server
  const response = await rp.post();
  if (response.statusCode !== httpStatus.ACCEPTED) {
    throw new HttpError(response.statusCode, 'error stopping server', response.body);
  }

  return serverId;
}

/**
 * @function updateMetadata
 * @description Adds, updates,or deletes metadata for the given
 *   server. If the 'value' is not given, the metadata id deleted.
 * @param {string} severId ID of server to update
 * @param {string} key metadata key
 * @param {string} value metadata value
 * @returns none
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function updateServerMetadata(serverId, key, value) {
  logger.debug('(serverId: %s, key: %s, value: %s)', serverId, key, value);

  const uri = apiConfig.uri.compute.updateMetadata
    .replace(':serverId', serverId)
    .replace(':key', key);
  logger.trace('uri: %s', uri);

  let validResponse;
  let response;

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token);

  // if value given, we are updating/adding; otherwise we are deleting
  if (typeof value !== 'undefined') {
    rp.body({ meta: { [key]: value } });
    validResponse = httpStatus.OK;
    response = await rp.put();
  } else {
    validResponse = httpStatus.NO_CONTENT;
    response = await rp.delete();
  }

  // check for correct result
  if (response.statusCode !== validResponse) {
    throw new HttpError(response.statusCode, 'error updating metadata', response.body);
  }
}

/**
 * @function initialize
 * @description Initializes the module. Functions are not directly
 *   exported. They are added to the export for the parent module.
 * @param  {object} config OpenStack config
 * @return none
 * @public
 */
function initialize(config) {
  logger.stringifyObjects(true);
  logger.debug();

  // OpenStack config (uri's, paths, etc...)
  apiConfig = config;
}

/**
 * module export(s)
 */

module.exports = {
  createServer,
  deleteServer,
  getFlavor,
  getFlavors,
  getMetadata,
  getServer,
  getServers,
  initialize,
  rebootServer,
  startServer,
  stopServer,
  updateServerMetadata
};
