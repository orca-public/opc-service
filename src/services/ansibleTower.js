/**
 * Ansible Tower API main module
 * @module ansibleTower
 */
/**
 * Copyright 2018 Virginia Polytechnic Institute and State University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * global(s) and required module(s)
 */

const API_NAME = 'AnsibleTower';
const WORKFLOW_JOB_TERMINAL_STATUS = ['successful', 'failed', 'error', 'canceled'];
const WORKFLOW_JOB_STATUS_FAILED = 'failed';

// Ansible Tower paths
const AT_PATH_AUTH = '/users/:userId/personal_tokens/';
const AT_PATH_CREATE_SERVER = '/workflow_job_templates/:templateId/launch/';
const AT_PATH_DELETE_SERVER = '/workflow_job_templates/:templateId/launch/';
const AT_PATH_GET_GROUP = '/inventories/:inventoryId/groups/?search=meta-vt_owner_:owner';
const AT_PATH_GET_HOST = '/hosts/?search=:serverId';
const AT_PATH_GET_HOSTS = '/groups/:groupId/hosts/';
const AT_PATH_GET_INVENTORY_VARS = '/inventories/:inventoryId/variable_data';
const AT_PATH_GET_JOB = '/jobs/:jobId/';
const AT_PATH_GET_WORKFLOW_JOB = '/workflow_jobs/:workflowJobId/';
const AT_PATH_GET_WORKFLOW_NODES = '/workflow_jobs/:workflowJobId/workflow_nodes/';
const AT_PATH_GET_WORKFLOW_TEMPLATE_NODES =
  '/workflow_job_templates/:workflowTemplateId/workflow_nodes/';
const AT_PATH_INSTALL_APPLICATION = '/job_templates/:jobTemplateId/launch/';
const AT_PATH_REVOKE_TOKEN = '/tokens/:userId/';
const AT_PATH_SERVER_ACTION = '/workflow_job_templates/:templateId/launch/';
const AT_PATH_UPDATE_METADATA = '/workflow_job_templates/:templateId/launch/';
const AT_PATH_VERIFY_TOKEN = '/me/';

// token renewal times (miniutes)
const TOKEN_FAIL_RETRY_MINUTES = 1;
const TOKEN_RENEWAL_MIN_MINUTES = 1;
const TOKEN_RENEWAL_MAX_MINUTES = 240;

const apputils = require('apputils');
const httpStatus = require('http-status-codes');
const logger = require('simlog').getLogger();

// required modules (local)
const atApi = require('./ansibleTowerApi');
const HttpError = require('../utils/HttpError');
const XError = require('../utils/XError');

// API config shared amoung the Ansible Tower modules; this is only updated
//   via this module even though all modules have access
const apiConfig = {
  // authentication info (token info)
  auth: {},
  name: 'OPC-API',
  // Ansible Tower REST endpoints
  uri: {}
};

let inventoryId = -1;
let serverCreateTemplateName = '';

// user info used to authenticate against Ansible Tower
let user = {};

/**
 * @function getWorkflowJobCount
 * @description Fetch the number of jobs that have completed for the
 *   given workflow template. We start at the beginning and count
 *   the jobs down the 'successful' path.
 * @param {number} workflowTemplateId workflow template ID
 * @return {number} number of 'successful' jobs
 * @private
 */
async function getWorkflowTemplateJobCount(workflowTemplateId) {
  logger.debug('{workflowTemplateId: %d}', workflowTemplateId);

  const nodes = await atApi.getWorkflowTemplateNodes(workflowTemplateId);

  // follow the 'successful' path through the workflow jobs and count them;
  //   we assume there is only 1 success node to follow at each node
  let count = 0;
  if (nodes && nodes.length) {
    let currentNode = nodes[0];
    count += 1;

    while (currentNode) {
      let nextNode = null;
      for (let i = 1; i < nodes.length; i += 1) {
        if (currentNode.success_nodes[0] === nodes[i].id) {
          nextNode = nodes[i];
          break;
        }
      }

      if (nextNode) {
        currentNode = nextNode;
        count += 1;
      } else {
        currentNode = null;
      }
    }
  }

  logger.debug(`{ count: ${count} }`);
  return count;
}

/**
 * @function setRenewTokenTimer
 * @description Sets the timer for the next token renewal. When
 *   timer fires, token is renewed and rescheduled again.
 *   NOTE: token renewal just extends the expiraton of the current
 *   token.
 * @param {number} waitTimeMills time to wait in milliseconds
 * @returns none
 * @private
 */
async function setRenewTokenTimer(waitTimeMills) {
  logger.debug(`{waitTimeMills: ${waitTimeMills}}`);

  setTimeout(async () => {
    try {
      apiConfig.auth = await atApi.authenticate(user);
      logger.debug('token: %s', apputils.obscureString(apiConfig.auth.token, -4));
      setRenewTokenTimer(waitTimeMills);
    } catch (error) {
      // if we fail, log it and try again sooner
      logger.error(error.message);
      setRenewTokenTimer(TOKEN_FAIL_RETRY_MINUTES * 60 * 1000);
    }
  }, waitTimeMills);
}

/**
 * @function validateToken
 * @description Validate the current token.
 * @return none
 */
async function validateToken() {
  logger.debug({ token: apputils.obscureString(apiConfig.auth.token, -4) });
  await atApi.validateToken();
}

/**
 * @function revokeToken
 * @description Revokes the current token.
 * @return ???
 */
async function revokeToken() {
  logger.debug({ token: apputils.obscureString(apiConfig.auth.token, -4) });
  await atApi.revokeToken(user);
}
/**
 * @function alterServerState
 * @description Changes the state of a server (start,stop,reboot).
 * @param {string} serverId ID of server to change
 * @param {string} action action to be performed
 * @return {number} workflow job ID
 * @throws {HttpError} if Ansible Tower error occurs
 */
async function alterServerState(serverId, action) {
  logger.debug({ serverId, action });
  const workflowJobId = await atApi.alterServerState(inventoryId, serverId, action);
  const workflowJobCount = await getWorkflowTemplateJobCount(
    apiConfig.workflowTemplateIds.serverAction
  );
  return { jobId: workflowJobId, jobCount: workflowJobCount };
}

/**
 * @function getInventoryVariables
 * @description Fetch the inventory variables for the given inventor ID.
 * @returns {object} inventory variables
 */
async function getInventoryVariables() {
  logger.debug();
  return atApi.getInventoryVariables();
}

/**
 * @function configureAnsibleTowerEndpoints
 * @description Builds the Ansible Tower URIs used by the API and
 *   fetch access token.
 * @param {string} baseUri base Ansible Tower URI
 * @return none
 */
async function configureAnsibleTowerApi(baseUri, tokenRenewalInterval) {
  logger.debug(`{baseUri: ${baseUri}, tokenRenewalInterval: ${tokenRenewalInterval}}`);

  apiConfig.name = apputils.getAppInfo('all').name;

  apiConfig.uri = {
    auth: baseUri + AT_PATH_AUTH.replace(':userId', user.id),
    getGroup: baseUri + AT_PATH_GET_GROUP.replace(':inventoryId', inventoryId),
    getHost: baseUri + AT_PATH_GET_HOST,
    getHosts: baseUri + AT_PATH_GET_HOSTS,
    getJob: baseUri + AT_PATH_GET_JOB,
    getInventoryVars: baseUri + AT_PATH_GET_INVENTORY_VARS.replace(':inventoryId', inventoryId),
    getWorkflowJobNodes: baseUri + AT_PATH_GET_WORKFLOW_NODES,
    getWorkflowJob: baseUri + AT_PATH_GET_WORKFLOW_JOB,
    getWorkflowTemplateNodes: baseUri + AT_PATH_GET_WORKFLOW_TEMPLATE_NODES,
    installApplication: baseUri + AT_PATH_INSTALL_APPLICATION,
    revokeToken: baseUri + AT_PATH_REVOKE_TOKEN,
    verifyToken: baseUri + AT_PATH_VERIFY_TOKEN
  };

  // get token to use with AT API calls

  // validate the token renewal interval
  const interval = parseInt(tokenRenewalInterval, 10);

  if (
    typeof interval !== 'number' ||
    interval < TOKEN_RENEWAL_MIN_MINUTES ||
    interval > TOKEN_RENEWAL_MAX_MINUTES
  ) {
    throw new XError(`invalid AT token renewal interval ${tokenRenewalInterval}`);
  }

  const tokenRenewalMills = interval * 60 * 1000;

  try {
    apiConfig.auth = await atApi.authenticate(user);
    logger.debug('token: %s', apputils.obscureString(apiConfig.auth.token, -4));
    setRenewTokenTimer(tokenRenewalMills);
  } catch (error) {
    // if we fail, log it and try again sooner
    logger.error(error.message);
    setRenewTokenTimer(TOKEN_FAIL_RETRY_MINUTES * 60 * 1000);
  }

  // get inventory variables
  const inventoryVars = await getInventoryVariables();

  // set template IDs from inventory
  apiConfig.workflowTemplateIds = inventoryVars.workflowTemplateIds;

  // set the name of the server creation template
  ({ serverCreateTemplateName } = inventoryVars);

  apiConfig.uri.createServer =
    baseUri +
    AT_PATH_CREATE_SERVER.replace(':templateId', apiConfig.workflowTemplateIds.serverCreate);

  apiConfig.uri.deleteServer =
    baseUri +
    AT_PATH_DELETE_SERVER.replace(':templateId', apiConfig.workflowTemplateIds.serverDestroy);

  apiConfig.uri.serverAction =
    baseUri +
    AT_PATH_SERVER_ACTION.replace(':templateId', apiConfig.workflowTemplateIds.serverAction);

  apiConfig.uri.updateMetadata =
    baseUri +
    AT_PATH_UPDATE_METADATA.replace(':templateId', apiConfig.workflowTemplateIds.updateMetadata);

  logger.debug(apiConfig.uri);
}

/**
 * @function createServer
 * @description Creates new server instance.
 * @param {object} args arguments for creating a server instance
 * {   instanceName: <name of the created server (optional)>,
 *            label: <label string>,
 *      description: <description string>,
 *        imageName: <name of the image to use>,
 *       flavorName: <name of the flavor to use>,
 *   expirationDate: <expiration (retire) timestamp>,
 *   securityGroups: <additional security groups>,
 *            owner: <instance owner> }
 * @return {number} workflow job ID
 * @public
 */
async function createServer(args) {
  logger.debug(args);
  const workflowJobId = await atApi.createServer(inventoryId, args);
  const workflowJobCount = await getWorkflowTemplateJobCount(
    apiConfig.workflowTemplateIds.serverCreate
  );
  return { jobId: workflowJobId, jobCount: workflowJobCount };
}

/**
 * @function deleteServer
 * @description Delete a server.
 * @param {string} serverId ID of server to delete
 * @return {number} workflow job ID
 * @throws {HttpError} if Ansible Tower error occurs
 */
async function deleteServer(serverId) {
  logger.debug({ serverId });
  const workflowJobId = await atApi.deleteServer(inventoryId, serverId);
  const workflowJobCount = await getWorkflowTemplateJobCount(
    apiConfig.workflowTemplateIds.serverDestroy
  );
  return { jobId: workflowJobId, jobCount: workflowJobCount };
}

/**
 * @function getCreateServerJobId
 * @description Given the workflow job ID for a 'create server' job,
 *   find the job ID for the child job that created the server.
 * @param {number} workflowJobId workflow job ID
 * @returns {number} job ID of the job that created the server
 * @throws {HttpError} if Ansible Tower error occurs
 * @private
 */
async function getCreateServerJobId(workflowJobId) {
  logger.debug({ workflowJobId });

  const results = await atApi.getWorkflowJobNodes(workflowJobId);

  // find the job ID for the job that created the server
  let jobId = null;
  for (let i = 0; i < results.length; i += 1) {
    const { job } = results[i].summary_fields;
    if (job && job.name === serverCreateTemplateName) {
      jobId = job.id;
      break;
    }
  }

  logger.trace({ jobId });
  return jobId;
}

/**
 * @function getJob
 * @description Gets information for the given simple job ID.
 * @param {number} jobId job ID
 * @returns {object} job information
 * @throws {HttpError} if Ansible Tower error occurs
 * @public
 */
async function getJob(jobId) {
  logger.debug({ jobId });

  const results = await atApi.getJob(jobId);

  const normalizedJob = {
    id: results.id,
    name: results.name,
    started: results.started ? new Date(results.started) : undefined,
    elapsed: results.elapsed ? parseFloat(results.elapsed) : undefined,
    finished: results.finished ? new Date(results.finished) : undefined,
    failed: results.failed,
    status: results.status,
    jobsCompleted: 0
  };

  if (WORKFLOW_JOB_TERMINAL_STATUS.includes(results.status)) {
    normalizedJob.jobsCompleted = 1;
  }

  logger.trace(normalizedJob);
  return normalizedJob;
}

/**
 * @function getServerId
 * @description Returns the server ID from the workflow job that
 *   created the server.
 * @param {number} jobId workflow job ID
 * @returns {string} server ID if found, null if not found
 * @throws {HttpError} if Ansible Tower error occurs
 * @public
 */
async function getServerId(jobId) {
  logger.debug({ jobId });

  let serverId = null;

  // find the job that created the server
  const childJobId = await getCreateServerJobId(jobId);

  // if job ID found, get the job info and pull out the server ID
  if (childJobId) {
    const job = await atApi.getJob(childJobId);
    if (job.artifacts && job.artifacts.os_instance) {
      serverId = job.artifacts.os_instance.id;
    }
  }
  // server was not created or invalid job ID
  if (!serverId) {
    throw new HttpError(httpStatus.NOT_FOUND, `server ID not found for job ${jobId}`);
  }
  logger.trace({ serverId });
  return serverId;
}

/**
 * @function normalizeServer
 * @description Normalizes the given server data.
 * @param {object} server server data to normalize
 * @return {object} normalized server data
 * @private
 */
function normalizeServer(serverVariables) {
  const variables = JSON.parse(serverVariables);

  const normalizedServer = {
    createdAt: variables.openstack.created ? new Date(variables.openstack.created) : undefined,
    flavor: variables.openstack.flavor,
    floatingIp: variables.ansible_ssh_host,
    floatingHost: variables.ansible_fqdn_external,
    id: variables.openstack.id,
    imageName: variables.openstack.image.name,
    name: variables.openstack.name,
    metadata: variables.openstack.metadata,
    status: variables.openstack.status.toLowerCase()
  };

  return normalizedServer;
}

/**
 * @function getServer
 * @description Fetch the server instances for the given owner.
 * @param {*} owner owner
 * @returns {array} server instances
 * @public
 */
async function getServer(serverId) {
  logger.debug(`{ serverId: ${serverId} }`);

  const server = await atApi.getServer(serverId);
  const normalizedServer = normalizeServer(server.variables);
  logger.trace(normalizedServer);
  return normalizedServer;
}

/**
 * @function getServers
 * @description Fetch the server instances for the given owner.
 * @param {*} owner owner
 * @returns {array} server instances
 * @public
 */
async function getServers(owner) {
  logger.debug(`{ owner: ${owner} }`);

  // find the job that created the server
  const servers = await atApi.getServers(owner);

  const normalizedServers = [];

  for (let i = 0; i < servers.length; i += 1) {
    normalizedServers.push(normalizeServer(servers[i].variables));
  }

  logger.trace(normalizedServers);
  return normalizedServers;
}

/**
 * @function getWorkflowJob
 * @description Gets information for the given workflow job ID.
 *   This call also returns information about the workflow child
 *   jobs. If one of the child jobs has failed, the workflow status
 *   is updated accordingly.
 * @param {number} workflowJobId workflow job ID
 * @returns {object} job information
 * @throws {HttpError} if Ansible Tower error occurs
 * @public
 */
async function getWorkflowJob(jobId) {
  logger.debug({ jobId });

  let results = await atApi.getWorkflowJob(jobId);

  const normalizedJob = {
    id: results.id,
    name: results.name,
    templateId: results.workflow_job_template
      ? parseInt(results.workflow_job_template, 10)
      : undefined,
    created: results.created ? new Date(results.created) : undefined,
    elapsed: results.elapsed ? parseFloat(results.elapsed) : undefined,
    finished: results.finished ? new Date(results.finished) : undefined,
    failed: results.failed,
    status: results.status,
    jobsCompleted: 0,
    jobs: []
  };

  // get status of child jobs; if child job failed, fail the workflow job
  results = await atApi.getWorkflowJobNodes(jobId);
  for (let i = 0; i < results.length; i += 1) {
    const { job } = results[i].summary_fields;
    if (job) {
      normalizedJob.jobs.push({ id: job.id, name: job.name, status: job.status });
      if (WORKFLOW_JOB_TERMINAL_STATUS.includes(job.status)) {
        normalizedJob.jobsCompleted += 1;
      }

      if (job.failed) {
        normalizedJob.failed = true;
        normalizedJob.status = WORKFLOW_JOB_STATUS_FAILED;
      }
    }
  }

  logger.trace(normalizedJob);
  return normalizedJob;
}

/**
 * @function health
 * @description Check that we can query Ansible Tower.
 * @param {object} healthResult check health result
 * @return none
 */
async function health(healthResult) {
  logger.debug();

  try {
    await validateToken();
    healthResult.resultUp(API_NAME);
    logger.debug(API_NAME, 'is UP');
  } catch (error) {
    logger.error(error.message);
    healthResult.resultDown(API_NAME);
  }
}

/**
 * @function initialize
 * @description Initializes the OpenStack API and performs the
 *   initial authentication. Also sets up the auto token renewal.
 * @return {[type]} [description]
 */
async function initialize(config) {
  logger.debug();
  logger.stringifyObjects(true);

  // save user info and inventory ID
  user = JSON.parse(JSON.stringify(config.user));
  ({ inventoryId } = config);

  // tell AT API what config to use and then configure the endpoints
  atApi.initialize(apiConfig);
  await configureAnsibleTowerApi(config.baseUri, config.tokenRenewalInterval);
}

/**
 * [installServerApplication description]
 * @return {[type]} [description]
 */
async function installApplication(serverId, jobTemplateId, args) {
  logger.debug('{serverId: %s, jobId: %s, args: %o}', serverId, jobTemplateId, args);
  const jobId = await atApi.installApplication(inventoryId, jobTemplateId, args);
  return { jobId, jobCount: 1 };
}

/**
 * @function updateServerMetadata
 * @description Updates the server metadata.
 * @param {string} serverId ID of server to update
 * @param {string} expirationDate expiration (retire) timestamp
 * @param {string} label server label
 * @param {string} description server description
 * @return {number} ID of the Ansible Tower update job
 * @throws {HttpError} if Ansible Tower error occurs
 * @public
 */
async function updateServerMetadata(serverId, owner, expirationDate, label, description) {
  logger.debug({ serverId, owner, expirationDate, label });
  const workflowJobId = await atApi.updateServerMetadata(
    inventoryId,
    serverId,
    owner,
    expirationDate,
    label,
    description
  );
  const workflowJobCount = await getWorkflowTemplateJobCount(
    apiConfig.workflowTemplateIds.updateMetadata
  );
  return { jobId: workflowJobId, jobCount: workflowJobCount };
}

/**
 * module export(s)
 */

module.exports = {
  alterServerState,
  createServer,
  deleteServer,
  getInventoryVariables,
  getJob,
  getServer,
  getServerId,
  getServers,
  getWorkflowJob,
  health,
  initialize,
  installApplication,
  revokeToken,
  updateServerMetadata
};
