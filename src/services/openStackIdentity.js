/**
 * OpenStack Identity API
 * @module openStackIdentity
 */
/**
 * Copyright 2018 Virginia Polytechnic Institute and State University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * global(s) and required module(s)
 */

const apputils = require('apputils');
const httpStatus = require('http-status-codes');
const logger = require('simlog').getLogger();
const simpleRp = require('simple-request-promise');

const HttpError = require('../utils/HttpError');

// OpenStack config; shared among OpenStack modules but only updated by
//   the parent module
let apiConfig = {};

/**
 * @function getUserAuthRequest
 * @description Returns the body for a user/password authentication
 *   request.
 * @returns {object}  authentication body
 * @private
 */
function getUserAuthRequest(user, project) {
  return {
    auth: {
      identity: {
        methods: ['password'],
        password: {
          user: { domain: { name: user.domain }, name: user.name, password: user.password }
        }
      },
      scope: {
        project: {
          domain: { name: project.domain },
          name: project.name
        }
      }
    }
  };
}

/**
 * @function authenticate
 * @description Authenticates a user against OpenStack.
 * @returns {object} authentication informaiton
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function authenticate(user, project) {
  if (logger.isLevelDebug()) {
    const tmp = JSON.parse(JSON.stringify(user));
    tmp.password = apputils.obscureString(5);
    logger.debug('(user: %o, project: %o)', tmp, project);
  }

  const uri = apiConfig.uri.identity.auth;
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.body(getUserAuthRequest(user, project));

  // authenticate
  const response = await rp.post();
  if (response.statusCode !== httpStatus.CREATED) {
    throw new HttpError(response.statusCode, 'authenication failed', response.body);
  }

  const tokenInfo = response.body.token;

  // token returned in header so add to the token info for return
  tokenInfo.token = response.headers[apiConfig.header.subjectToken];

  if (logger.isLevelTrace()) {
    const tmp = JSON.parse(JSON.stringify(tokenInfo));
    tmp.token = apputils.obscureString(tmp.token, -4);
    logger.trace(tmp);
  }

  return tokenInfo;
}

/**
 * @function revokeToken
 * @description Revokes the given token.
 * @returns none
 * @public
 */
async function revokeToken() {
  logger.debug('(token: %s)', apputils.obscureString(apiConfig.auth.token, -4));

  const uri = apiConfig.uri.identity.auth;
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token).header(
    apiConfig.header.subjectToken,
    apiConfig.auth.token
  );

  // request token revocation from OpenStack (note docs say it returns a 201)
  const response = await rp.delete();
  if (response.statusCode !== httpStatus.NO_CONTENT) {
    logger.warn(`error revoking token (${response.statusCode})`);
  }
}

/**
 * @function validateToken
 * @description Validates the current token.
 * @return {[type]} [description]
 * @throws {HttpError} if OpenStack error occurs
 */
async function validateToken() {
  logger.debug('(token: %s)', apputils.obscureString(apiConfig.auth.token, -4));

  const uri = apiConfig.uri.identity.auth;
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header(apiConfig.header.authToken, apiConfig.auth.token).header(
    apiConfig.header.subjectToken,
    apiConfig.auth.token
  );

  // request token verification
  const response = await rp.head();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'token validation failed', response.body);
  }
}

/**
 * @function initialize
 * @description Initializes the module. Functions are not directly
 *   exported. They are added to the export for the parent module.
 * @param  {object} config OpenStack config
 * @return {object} containing function exports
 * @public
 */
function initialize(config) {
  logger.stringifyObjects(true);
  logger.debug();

  // OpenStack config (token, uri's, paths, etc...)
  apiConfig = config;
}

/**
 * module export(s)
 */

module.exports = {
  authenticate,
  initialize,
  revokeToken,
  validateToken
};
