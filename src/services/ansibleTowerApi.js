/**
 * Ansible Tower API
 * @module ansibleTowerApi
 */
/**
 * Copyright 2018 Virginia Polytechnic Institute and State University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * global(s) and required module(s)
 */

const apputils = require('apputils');
const httpStatus = require('http-status-codes');
const logger = require('simlog').getLogger();
const simpleRp = require('simple-request-promise');

const HttpError = require('../utils/HttpError');

// AnsibleTower config; shared among Ansible Tower modules but only updated by
//   the parent module
let apiConfig = {};

/**
 * @function alterServerState
 * @description Changes the state of a server (start,stop,reboot).
 * @param {string} serverId ID of server to change
 * @param {string} action action to be performed
 * @return {number} workflow job ID
 * @throws {HttpError} if Ansible Tower error occurs
 */
async function alterServerState(inventoryId, serverId, action) {
  logger.debug('{inventoryId: %d, serverId: %s, action: %s}', inventoryId, serverId, action);

  const uri = apiConfig.uri.serverAction;
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header('Authorization', `Bearer ${apiConfig.auth.token}`).body({
    inventory: inventoryId,
    extra_vars: { instance_id: serverId, server_action: action }
  });

  // perform action on server
  const response = await rp.post();
  if (response.statusCode !== httpStatus.CREATED) {
    throw new HttpError(response.statusCode, 'error performing server action', response.body);
  }

  const jobId = response.body.id;
  logger.trace({ jobId });
  return jobId;
}

/**
 * @function authenticate
 * @description Authenticates a user against Ansible Tower.
 * @returns {object} authentication information
 * @throws {} if Ansible Tower error occurs
 * @public
 */
async function authenticate(user) {
  logger.debug();

  const uri = apiConfig.uri.auth;
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  const creds = `${user.name}:${user.password}`;
  rp.header('Authorization', `Basic ${Buffer.from(creds).toString('base64')}`);
  rp.body({ description: apiConfig.name, scope: 'write' });

  // authenticate
  const response = await rp.post();
  if (response.statusCode !== httpStatus.CREATED) {
    throw new HttpError(response.statusCode, 'error authenticating', response.body);
  }

  const auth = response.body;

  if (logger.isLevelTrace()) {
    const tmp = JSON.parse(JSON.stringify(auth));
    tmp.token = apputils.obscureString(tmp.token, -4);
    tmp.refresh_token = apputils.obscureString(tmp.refresh_token, -4);
    logger.trace(tmp);
  }

  return auth;
}

/**
 * @function createServer
 * @description Creates an OPC server.
 * @param {object} args arguments for creating a server instance
 * {   instanceName: <name of the created server (optional)>,
 *            label: <label string>,
 *      description: <description string>,
 *        imageName: <name of the image to use>,
 *       flavorName: <name of the flavor to use>,
 *   expirationDate: <expiration (retire) timestamp>,
 *   securityGroups: <additional security groups>,
 *   usersAndGroups: <users/groups allowed SSH access>,
 *            owner: <instance owner> }
 * @return {number} workflow job ID
 * @public
 */
async function createServer(inventoryId, args) {
  logger.debug('{inventoryId: %d, args: %o}', inventoryId, args);

  const uri = apiConfig.uri.createServer;
  logger.trace('uri: %s', uri);

  const body = {
    inventory: inventoryId,
    extra_vars: {
      image_name: args.imageName,
      flavor_name: args.flavorName,
      instance_label: args.label,
      instance_description: args.description,
      instance_owner: args.owner,
      add_sshd_allow_groups: `[${args.usersAndGroups}]`,
      add_sudo_groups: `[{name: ${args.owner}, defaults: '!requiretty'}]`,
      instance_retire_date: args.expirationDate
    }
  };

  // server name is optional
  if (args.instanceName) {
    body.extra_vars.instance_name = args.instanceName;
  }

  // add security groups if given
  if (args.securityGroups) {
    body.extra_vars.add_security_groups = `[${args.securityGroups}]`;
  }

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header('Authorization', `Bearer ${apiConfig.auth.token}`).body(body);

  // create instance
  const response = await rp.post();
  if (response.statusCode !== httpStatus.CREATED) {
    throw new HttpError(response.statusCode, 'error creating server', response.body);
  }

  const jobId = response.body.id;
  logger.trace({ jobId });
  return jobId;
}

/**
 * @function deleteServer
 * @description Creates and OPC server.
 * @param {string} serverId ID of server to delete
 * @return {number} workflow job ID
 * @throws {HttpError} if Ansible Tower error occurs
 */
async function deleteServer(inventoryId, serverId) {
  logger.debug('{inventoryId: %d, serverId: %s}', inventoryId, serverId);

  const uri = apiConfig.uri.deleteServer;
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header('Authorization', `Bearer ${apiConfig.auth.token}`).body({
    inventory: inventoryId,
    extra_vars: { instance_id: serverId }
  });

  // fire off delete job
  const response = await rp.post();
  if (response.statusCode !== httpStatus.CREATED) {
    throw new HttpError(response.statusCode, 'error deleting server', response.body);
  }

  const jobId = response.body.id;
  logger.trace({ jobId });
  return jobId;
}

/**
 * @function getInventoryVariables
 * @description Fetch the inventory variables for given inventory ID.
 * @param {} inventoryId inventory ID
 * @returns {object} inventory variables
 * @public
 */
async function getInventoryVariables() {
  logger.debug();

  const uri = apiConfig.uri.getInventoryVars;
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header('Authorization', `Bearer ${apiConfig.auth.token}`);

  // get the job info
  const response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'error getting inventory variables', response.body);
  }

  logger.trace(response.body);
  return response.body;
}

/**
 * @function getJob
 * @description Returns information for the given job ID.
 * @param {number} jobId job ID
 * @returns {object} job information
 * @throws {HttpError} if Ansible Tower error occurs
 * @public
 */
async function getJob(jobId) {
  logger.debug('(jobId: %d', jobId);

  const uri = apiConfig.uri.getJob.replace(':jobId', jobId);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header('Authorization', `Bearer ${apiConfig.auth.token}`);

  // get the job info
  const response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'error getting job', response.body);
  }

  // get the job info
  const job = response.body;
  logger.trace(job);
  return job;
}

/**
 * @function getServer
 * @description Fetch the server info for the given server ID.
 * @param {*} serverId server ID
 * @returns server info
 * @public
 */
async function getServer(serverId) {
  logger.debug(`{serverId: ${serverId}}`);

  // setup request
  const uri = apiConfig.uri.getHost.replace(':serverId', serverId);
  logger.trace('uri: %s', uri);
  const rp = simpleRp.getInstance(uri);
  rp.header('Authorization', `Bearer ${apiConfig.auth.token}`);

  // get the server info
  const response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'error getting server', response.body);
  }

  // make sure something was found
  if (response.body.count === 0) {
    throw new HttpError(httpStatus.NOT_FOUND, `server not found (${serverId})`);
  }

  const server = response.body.results[0];
  logger.trace(server);
  return server;
}

/**
 * @function getServers
 * @description Fetch list of server instances for the given owner.
 * @param {*} owner owner to search for
 * @returns server instances for the given owner
 * @public
 */
async function getServers(owner) {
  logger.debug(`{owner: ${owner}}`);

  // setup group request
  let uri = apiConfig.uri.getGroup.replace(':owner', owner);
  logger.trace('uri: %s', uri);
  let rp = simpleRp.getInstance(uri);
  rp.header('Authorization', `Bearer ${apiConfig.auth.token}`);

  // get the owner's group
  let response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'error getting owner group', response.body);
  }

  let servers = [];

  // should only be one group
  if (response.body.count !== 1) {
    logger.debug(`'no group found for ${owner}`);
  } else {
    const groupId = response.body.results[0].id;

    // setup server request
    uri = apiConfig.uri.getHosts.replace(':groupId', groupId);
    logger.trace('uri: %s', uri);
    rp = simpleRp.getInstance(uri);
    rp.header('Authorization', `Bearer ${apiConfig.auth.token}`);

    // get the server info
    response = await rp.get();
    if (response.statusCode !== httpStatus.OK) {
      throw new HttpError(response.statusCode, 'error getting servers', response.body);
    }

    // get the servers
    servers = response.body.results;
  }

  logger.trace(servers);
  return servers;
}

/**
 * @function getWorkflowJob
 * @description Gets information for the given workflow job ID.
 *   This call also returns information about the workflow child
 *   jobs. If one of the child jobs has failed, the workflow status
 *   is updated accordingly.
 * @param {number} workflowJobId workflow job ID
 * @returns {object} job information
 * @throws {HttpError} if Ansible Tower error occurs
 * @public
 */
async function getWorkflowJob(workflowJobId) {
  logger.debug('(workflowJobId: %d)', workflowJobId);

  const uri = apiConfig.uri.getWorkflowJob.replace(':workflowJobId', workflowJobId);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header('Authorization', `Bearer ${apiConfig.auth.token}`);

  // fetch job info
  const response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'error getting workflow job', response.body);
  }

  const results = response.body;
  logger.trace(results);
  return results;
}

/**
 * @function getWorkflowJobNodes
 * @description Return the nodes for a workflow job.
 * @param {number} workflowJobId ID of workflow job
 * @returns {array} node information
 * @throws {HttpError} if Ansible Tower error occurs
 * @private
 */
async function getWorkflowJobNodes(workflowJobId) {
  logger.debug('(workflowJobId: %d)', workflowJobId);

  const uri = apiConfig.uri.getWorkflowJobNodes.replace(':workflowJobId', workflowJobId);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header('Authorization', `Bearer ${apiConfig.auth.token}`);

  // get job nodes
  const response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'error getting workflow job nodes', response.body);
  }

  const { results } = response.body;
  logger.trace(results);
  return results;
}

/**
 * @function getWorkflowTemplateNodes
 * @description Return the nodes for a workflow template.
 * @param {number} workflowTemplateId ID of workflow template
 * @returns {array} node information
 * @throws {HttpError} if Ansible Tower error occurs
 * @public
 */
async function getWorkflowTemplateNodes(workflowTemplateId) {
  logger.debug('{workflowTemplateId: %d}', workflowTemplateId);

  const uri = apiConfig.uri.getWorkflowTemplateNodes.replace(
    ':workflowTemplateId',
    workflowTemplateId
  );
  logger.trace('{uri: %s}', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header('Authorization', `Bearer ${apiConfig.auth.token}`);

  // get job nodes
  const response = await rp.get();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(
      response.statusCode,
      'error getting workflow template nodes',
      response.body
    );
  }

  const { results } = response.body;
  logger.trace(results);
  return results;
}

/**
 * @function installApplication
 * @description Installs an application on the specified server.
 *   The 'jobTemplateId' is the Ansible Tower job to fire off and is
 *   specefic to the application being installed.
 * @param {number} jobTemplateId ID of the job template to fire off
 * @param {object} jobArgs job arguments
 * @return {number} job ID
 * @public
 */
async function installApplication(inventoryId, jobTemplateId, jobArgs) {
  logger.debug(
    '{jobTemplateId: %d, inventoryId: %d, jobArgs: %o}',
    jobTemplateId,
    inventoryId,
    jobArgs
  );

  const uri = apiConfig.uri.installApplication.replace(':jobTemplateId', jobTemplateId);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header('Authorization', `Bearer ${apiConfig.auth.token}`).body({
    inventory: inventoryId,
    extra_vars: {
      host: jobArgs.host,
      application_admins: `[${jobArgs.application_admins}]`,
      application_allow_groups: `[${jobArgs.application_allow_groups}]`
    }
  });

  // perform action on server
  const response = await rp.post();
  if (response.statusCode !== httpStatus.CREATED) {
    throw new HttpError(response.statusCode, 'error installing application', response.body);
  }

  const jobId = response.body.id;
  logger.trace({ jobId });
  return jobId;
}

/**
 * @function revokeToken
 * @description Revokes the current token.
 * @return none
 */
async function revokeToken(user) {
  logger.debug('(token: %s)', apputils.obscureString(apiConfig.auth.token, -4));

  const uri = apiConfig.uri.revokeToken.replace(':userId', apiConfig.auth.id);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  const creds = `${user.name}:${user.password}`;
  rp.header('Authorization', `Basic ${Buffer.from(creds).toString('base64')}`);

  // request token revocation
  const response = await rp.delete();
  if (response.statusCode !== httpStatus.ACCEPTED) {
    logger.warn(`error revoking token (${response.statusCode})`);
  }
}

/**
 * @function updateServerMetadata
 * @description Updates the server expiration (retire) date.
 * @param {string} serverId ID of server to update
 * @param {string} owner instance owner
 * @param {string} expirationDate expiration timestamp
 * @param {string} label instance label
 * @param {string} description instance description
 * @return {number} ID of the Ansible Tower update job
 * @throws {HttpError} if Ansible Tower error occurs
 * @public
 */
async function updateServerMetadata(
  inventoryId,
  serverId,
  owner,
  expirationDate,
  label,
  description
) {
  logger.debug(
    '{inventoryId: %d, serverId: %s, owner: %s, expirationDate: %s, label: %s, description: %s}',
    inventoryId,
    serverId,
    owner,
    expirationDate,
    label,
    description
  );

  const uri = apiConfig.uri.updateMetadata;
  logger.trace('uri: %s', uri);

  const body = {
    inventory: inventoryId,
    extra_vars: {
      instance_id: serverId,
      instance_owner: owner,
      instance_retire_date: expirationDate,
      instance_label: label,
      instance_description: description
    }
  };

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header('Authorization', `Bearer ${apiConfig.auth.token}`).body(body);

  // fire off update
  const response = await rp.post();
  if (response.statusCode !== httpStatus.CREATED) {
    throw new HttpError(response.statusCode, 'error updating metadata', response.body);
  }

  const jobId = response.body.id;
  logger.trace({ jobId });
  return jobId;
}

/**
 * @function validateToken
 * @description Validate the current token.
 * @return none
 */
async function validateToken() {
  logger.debug('(token: %s)', apputils.obscureString(apiConfig.auth.token, -4));

  const uri = apiConfig.uri.verifyToken.replace(':userId', apiConfig.auth.id);
  logger.trace('uri: %s', uri);

  // setup request
  const rp = simpleRp.getInstance(uri);
  rp.header('Authorization', `Bearer ${apiConfig.auth.token}`);

  // request token verification
  const response = await rp.head();
  if (response.statusCode !== httpStatus.OK) {
    throw new HttpError(response.statusCode, 'error validating token', response.body);
  }
}

/**
 * @function initialize
 * @description Initializes the OpenStack API and performs the
 *   initial authentication. Also sets up the auto token renewal.
 * @return {[type]} [description]
 */
async function initialize(config) {
  logger.stringifyObjects(true);
  logger.debug();

  // Ansible Tower config (uri's, paths, etc...)
  apiConfig = config;
}

/**
 * module export(s)
 */

module.exports = {
  alterServerState,
  authenticate,
  createServer,
  deleteServer,
  getInventoryVariables,
  getJob,
  getServer,
  getServers,
  getWorkflowJob,
  getWorkflowJobNodes,
  getWorkflowTemplateNodes,
  initialize,
  installApplication,
  revokeToken,
  updateServerMetadata,
  validateToken
};
