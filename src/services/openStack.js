/**
 * OpenStack API main module
 * @module openStack
 */
/**
 * Copyright 2018 Virginia Polytechnic Institute and State University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * global(s) and required module(s)
 */

const API_NAME = 'OpenStack';

// OpenStack paths
const OS_PATH_AUTH = '/auth/tokens';
const OS_PATH_CREATE_SERVER = '/servers';
const OS_PATH_DELETE_SERVER = '/servers/:serverId';
const OS_PATH_GET_FLAVOR = '/flavors/:flavorId';
const OS_PATH_GET_FLAVORS = '/flavors/detail';
const OS_PATH_GET_IMAGE = '/images/:imageId';
const OS_PATH_GET_IMAGES = '/images?status=:status';
const OS_PATH_GET_METADATA = '/servers/:serverId/metadata';
const OS_PATH_GET_NETWORK = '/networks?name=:networkName';
const OS_PATH_GET_SERVER = '/servers/:serverId';
const OS_PATH_GET_SERVERS = '/servers/detail';
const OS_PATH_SERVER_ACTION = '/servers/:serverId/action';
const OS_PATH_UPDATE_METADATA = '/servers/:serverId/metadata/:key';

// token renewal times (minutes)
const TOKEN_FAIL_RETRY_MINUTES = 1;
const TOKEN_RENEWAL_DEFAULT_MINUTES = 45;
const TOKEN_RENEWAL_MIN_MINUTES = 1;
const TOKEN_RENEWAL_MAX_MINUTES = 60;

const apputils = require('apputils');
const logger = require('simlog').getLogger();

const osCompute = require('./openStackCompute');
const osIdentity = require('./openStackIdentity');
const osImage = require('./openStackImage');
const osNetwork = require('./openStackNetwork');

// API config shared amoung the OpenStack modules; this is only updated
//   via this module even though all modules have access
const apiConfig = {
  // authentication info (token, user, project, etc...)
  auth: {},
  // header values used to send requests to OpenStack
  header: {
    authToken: 'x-auth-token',
    subjectToken: 'x-subject-token'
  },
  // OpenStack REST endpoints
  uri: {}
};

// user and project info used to authenticate against OpenStack
let user = {};
let project = {};

let osTimer = null;

/**
 * @function authenticate
 * @description Authenticates against OpenStack and updates the
 *   global auth config used by the various modules.
 * @return none
 * @private
 */
async function authenticate() {
  logger.debug();
  const tokenInfo = await osIdentity.authenticate(user, project);

  apiConfig.auth = {
    token: tokenInfo.token,
    expirationDate: new Date(tokenInfo.expires_at),
    project: tokenInfo.project,
    user: tokenInfo.user
  };

  if (logger.isLevelDebug()) {
    logger.debug('token: %s', apputils.obscureString(apiConfig.auth.token, -4));
  }
}

/**
 * @function configureOpenStackEndpoints
 * @param  {[type]} baseUri [description]
 * @return {[type]}         [description]
 */
function configureOpenStackEndpoints(baseUri) {
  logger.debug();

  apiConfig.uri.compute = {
    createServer: baseUri.compute + OS_PATH_CREATE_SERVER,
    deleteServer: baseUri.compute + OS_PATH_DELETE_SERVER,
    getFlavor: baseUri.compute + OS_PATH_GET_FLAVOR,
    getFlavors: baseUri.compute + OS_PATH_GET_FLAVORS,
    getMetadata: baseUri.compute + OS_PATH_GET_METADATA,
    getServer: baseUri.compute + OS_PATH_GET_SERVER,
    getServers: baseUri.compute + OS_PATH_GET_SERVERS,
    serverAction: baseUri.compute + OS_PATH_SERVER_ACTION,
    updateMetadata: baseUri.compute + OS_PATH_UPDATE_METADATA
  };

  apiConfig.uri.identity = {
    auth: baseUri.identity + OS_PATH_AUTH
  };

  apiConfig.uri.image = {
    getImage: baseUri.image + OS_PATH_GET_IMAGE,
    getImages: baseUri.image + OS_PATH_GET_IMAGES
  };

  apiConfig.uri.network = {
    getNetwork: baseUri.network + OS_PATH_GET_NETWORK
  };

  logger.debug(apiConfig.uri);
}

/**
 * @function createServer
 * @description Create a server in OpenStack.
 * @param  {object} args needed arguments to create server
 *   {     serverName: <name to associate with new server>,
 *            imageId: <ID of image to create instance from>,
 *           flavorId: <instance flavor (RAM size, disk size, etc.)>,
 *          networkId: <ID of network instance will connect to>,
 *        keyPairName: <SSH keypair to allow access (optional)>,
 *           metadata: <object with key/value pairs (optional)>,
 *     securityGroups: <array of security access groups (optional)> }
 * @return {string} ID of the new server
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function createServer(args) {
  logger.debug('(%o)', args);
  return osCompute.createServer(args);
}

/**
 * @function deleteServer
 * @description Deletes the given server.
 * @param {string} serverId ID of server to delete
 * @return {string} ID of server deleted
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function deleteServer(serverId) {
  logger.debug('(serverId: %s)', serverId);
  await osCompute.deleteServer(serverId);
}

/**
 * @function normalizeAddress
 * @description Normalizes the given server IP addresses data.
 * @param {array} addrData address data to normallize
 * @return {array} normalized data
 * @throws {HttpError} if OpenStack error occurs
 * @private
 */
async function normalizeAddresses(name, addresses) {
  const normalizedData = [];

  // the network names are the properties
  const networkNames = Object.keys(addresses);

  // process each network
  for (let i = 0; i < networkNames.length; i += 1) {
    const networkData = addresses[networkNames[i]];

    for (let j = 0; j < networkData.length; j += 1) {
      const type = networkData[j]['OS-EXT-IPS:type'];
      const info = {
        networkName: networkNames[i],
        address: networkData[j].addr,
        type,
        version: networkData[j].version
      };

      // if address type is floating, we determine/build the hostname;
      //   NOTE: currently, there is supposed to only be one floating IP
      //   per server
      if (type === 'floating') {
        // eslint-disable-next-line no-await-in-loop
        const data = await osNetwork.getNetwork(networkNames[i]);
        if (data.networks.length > 0) {
          info.hostname = `${name}.${data.networks[0].dns_domain.slice(0, -1)}`;
        }
      }

      // save the network info
      normalizedData.push(info);
    }
  }

  return normalizedData;
}

/**
 * @function normalizeFlavor
 * @description Normalizes the given flavor data.
 * @param {object} flavor flavor data to normalize
 * @return {object} normalized flavor data
 * @private
 */
function normalizeFlavor(flavor) {
  return {
    diskSize: parseInt(flavor.disk, 10),
    id: flavor.id,
    name: flavor.name,
    ram: parseInt(flavor.ram, 10),
    swap: flavor.swap ? parseInt(flavor.swap, 10) : 0,
    vcpus: parseInt(flavor.vcpus, 10)
  };
}

/**
 * @function normalizeImage
 * @description Normalizes the given image data.
 * @param {object} image image data to normalize
 * @return {object} normalized image data
 * @private
 */
function normalizeImage(image) {
  return {
    createdAt: image.created_at ? new Date(image.created_at) : undefined,
    id: image.id,
    minDisk: image.min_disk ? parseInt(image.min_disk, 10) : 0,
    minRam: image.min_ram ? parseInt(image.min_ram, 10) : 0,
    name: image.name,
    size: image.size ? parseInt(image.size, 10) : 0,
    status: image.status,
    updatedAt: image.updated_at ? new Date(image.updated_at) : undefined
  };
}

/**
 * @function normalizeServer
 * @description Normalizes the given server data.
 * @param {object} server server data to normalize
 * @return {object} normalized server data
 * @private
 */
function normalizeServer(server, flavor, addresses) {
  const normalizedServer = {
    id: server.id,
    createdAt: server.created ? new Date(server.created) : undefined,
    launchedAt: server['OS-SRV-USG:launched_at']
      ? new Date(server['OS-SRV-USG:launched_at'])
      : undefined,
    addresses,
    fault: server.fault,
    flavor,
    metadata: server.metadata,
    name: server.name,
    securityGroups: server.security_groups,
    status: server.status.toLowerCase(),
    tenantId: server.tenant_id,
    userId: server.user_id
  };

  for (let i = 0; i < addresses.length; i += 1) {
    if (addresses[i].type === 'floating') {
      normalizedServer.floatingIp = addresses[i].address;
      normalizedServer.floatingHost = addresses[i].hostname;
      break;
    }
  }

  return normalizedServer;
}

/**
 * @function getFlavor
 * @description Returns details for the given flavor.
 * @param {string} flavorId ID of flavor to query
 * @return {object} flavor details
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getFlavor(flavorId) {
  logger.debug('(flavorId: %s)', flavorId);
  const flavor = await osCompute.getFlavor(flavorId);
  const normalizedFlavor = normalizeFlavor(flavor);
  logger.trace(normalizedFlavor);
  return normalizedFlavor;
}

/**
 * @function getFlavors
 * @description Returns available flavors.
 * @return {array} flavors
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getFlavors() {
  logger.debug();
  const flavors = await osCompute.getFlavors();

  const normalizedFlavors = [];
  for (let i = 0; i < flavors.length; i += 1) {
    normalizedFlavors.push(normalizeFlavor(flavors[i]));
  }

  logger.trace(normalizedFlavors);
  return normalizedFlavors;
}

/**
 * @function getImage
 * @description Return details for the given image.
 * @param {string} imageId ID of image to query
 * @return {object} image details
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getImage(imageId) {
  logger.debug('(imageId: %s)', imageId);
  const image = await osImage.getImage(imageId);
  const normalizedImage = normalizeImage(image);
  logger.trace(normalizedImage);
  return normalizedImage;
}

/**
 * @function getImages
 * @description Returns available images.
 * @return {array} available images
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getImages() {
  logger.debug();
  const images = await osImage.getImages();

  const normalizedData = [];
  for (let i = 0; i < images.length; i += 1) {
    normalizedData.push(normalizeImage(images[i]));
  }

  logger.trace(normalizedData);
  return normalizedData;
}

/**
 * @function getServer
 * @description Return details for the given server.
 * @param {string} serverId ID of server to query
 * @return {object} server details
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getServer(serverId) {
  logger.debug('(serverId: %s)', serverId);
  const server = await osCompute.getServer(serverId);
  const addrData = await normalizeAddresses(server.name, server.addresses);
  const flavor = await getFlavor(server.flavor.id);
  const normalizedServer = normalizeServer(server, flavor, addrData);
  logger.trace(normalizedServer);
  return normalizedServer;
}

/**
 * @function getServerMetadata
 * @description Returns metdata for the given server.
 * @param {string} serverId ID of server to fetch metadata for
 * @return {object} metadata
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getServerMetadata(serverId) {
  logger.debug('(serverId: %s)', serverId);
  const metadata = await osCompute.getMetadata(serverId);
  logger.trace(metadata);
  return metadata;
}

/**
 * @function getServers
 * @description Returns instantiated servers for the given owner.
 * @param {string} owner owner of the servers to query for
 * @return {array} servers
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getServers(owner) {
  logger.debug('(owner: %s)', owner);
  const servers = await osCompute.getServers();

  const normalizedServers = [];
  for (let i = 0; i < servers.length; i += 1) {
    // if owner given, they must match the 'vt_owner' metadata
    if (!(owner && (!servers[i].metadata.vt_owner || servers[i].metadata.vt_owner !== owner))) {
      // eslint-disable-next-line no-await-in-loop
      const addrData = await normalizeAddresses(servers[i].name, servers[i].addresses);

      // eslint-disable-next-line no-await-in-loop
      const flavor = await getFlavor(servers[i].flavor.id);

      normalizedServers.push(normalizeServer(servers[i], flavor, addrData));
    }
  }

  logger.trace(normalizedServers);
  return normalizedServers;
}

/**
 * @function getServerStatus
 * @description Returns the status for the given server.
 * @param {string} serverId ID of server to query
 * @return {string} server status: active,build,deleted,error,
 *   hard_reboot,migrating,password,paused,reboot,rebuild,
 *   rescue,resize,reverte_resize,shelved,shelved_offloaded,
 *   shutoff,soft_deleted,suspended,unknown,verify_resize
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function getServerStatus(serverId) {
  logger.debug('(serverId: %s)', serverId);
  const server = await osCompute.getServer(serverId);
  const status = server.status.toLowerCase();
  logger.trace('status:', status);
  return status;
}

/**
 * @function health
 * @description Check that we can query to the OpenStack API.
 * @param {object} healthResult check health result
 * @return none
 * @public
 */
async function health(healthResult) {
  logger.debug();

  try {
    await osIdentity.validateToken();
    healthResult.resultUp(API_NAME);
    logger.debug(API_NAME, 'is UP');
  } catch (error) {
    logger.error(error.message);
    healthResult.resultDown(API_NAME);
  }
}

/**
 * @function setRenewTokenTimer
 * @description Sets the timer for the next token renewal. When
 *   timer fires, token is renewed and rescheduled again.
 * @param {number} waitTimeMills time to wait in milliseconds
 * @returns none
 * @private
 */
async function setRenewTokenTimer(waitTimeMills) {
  logger.debug('(waitTimeMills: %d)', waitTimeMills);

  osTimer = setTimeout(async () => {
    logger.debug();
    try {
      await authenticate();
      setRenewTokenTimer(waitTimeMills);
    } catch (error) {
      // if we fail, log it and try again sooner
      logger.error(error.message);
      setRenewTokenTimer(TOKEN_FAIL_RETRY_MINUTES * 60 * 1000);
    }
  }, waitTimeMills);
}

/**
 * @function initialize
 * @description Initializes the OpenStack API and performs the
 *   initial authentication. Also sets up the auto token renewal.
 * @return none
 * @public
 */
async function initialize(config, standalone) {
  logger.stringifyObjects(true);

  if (logger.isLevelDebug()) {
    const tmp = JSON.parse(JSON.stringify(config));
    tmp.user.password = apputils.obscureString(5);
    logger.debug('(%o,%s)', tmp, standalone || 'false');
  }

  // save OpenStack config
  configureOpenStackEndpoints(config.baseUri);
  apiConfig.baseUri = JSON.parse(JSON.stringify(config.baseUri));
  user = JSON.parse(JSON.stringify(config.user));
  project = JSON.parse(JSON.stringify(config.project));

  // initialize sub-modules
  osCompute.initialize(apiConfig);
  osIdentity.initialize(apiConfig);
  osImage.initialize(apiConfig);
  osNetwork.initialize(apiConfig);

  // if standalone use, we don't need to renew the token
  if (standalone) {
    await authenticate();
  } else {
    // validate the token renewal interval
    let intervalMinutes = parseInt(config.tokenRenewalInterval, 10);
    if (
      typeof intervalMinutes === 'number' ||
      intervalMinutes < TOKEN_RENEWAL_MIN_MINUTES ||
      intervalMinutes > TOKEN_RENEWAL_MAX_MINUTES
    ) {
      intervalMinutes = TOKEN_RENEWAL_DEFAULT_MINUTES;
    }

    // authenticate and fire off renewal timer
    try {
      await authenticate();
      setRenewTokenTimer(intervalMinutes * 60 * 1000);
    } catch (error) {
      // if we fail, log it and try again sooner
      logger.error(error.message);
      setRenewTokenTimer(TOKEN_FAIL_RETRY_MINUTES * 1000);
    }
  }
}

/**
 * @function isOwner
 * @description Returns true if the given username is the 'owner' of
 *   the given server.
 * @param {string} username username to verify ownership
 * @param {string} serverId ID of server to verify against
 * @return {boolean} true if username is the 'owner', false otherwise
 */
async function isOwner(username, serverId) {
  logger.debug('(user: %s,serverId: %s)', username, serverId);
  const metadata = await getServerMetadata(serverId);
  const owner = metadata.vt_owner && metadata.vt_owner === username;
  logger.trace('owner?', owner);
  return owner;
}

/**
 * @function rebootServer
 * @description Reboots the given server. A HARD reboot is like
 *   power cycling the machine. A SOFT reboot is a normal, graceful
 *   reboot.
 * @param {string} serverId ID of server to reboot
 * @param {string} rebootType type of reboot: HARD or SOFT (default)
 * @return {string} ID of rebooted server
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function rebootServer(serverId, rebootType) {
  const type = rebootType ? rebootType.toUpperCase() : 'SOFT';
  logger.debug('(serverId: %s, type: %s)', serverId, type);
  await osCompute.rebootServer(serverId, type);
}

/**
 * @function revokeToken
 * @description Revoke the currently active token.
 * @throws {HttpError} if OpenStack error encountered
 * @return none
 * @public
 */
async function revokeToken() {
  logger.debug();
  if (osTimer) {
    clearTimeout(osTimer);
    osTimer = null;
  }
  await osIdentity.revokeToken();
  apiConfig.auth = {};
}

/**
 * @function startServer
 * @description Starts the given server.
 * @param {string} serverId ID of server to start
 * @return {string} ID of started server
 * @throws {HttpError} if OpenStack error occurs
 *   409 (conflict) if server status != 'SHUTOFF' (?)
 * @public
 */
async function startServer(serverId) {
  logger.debug('(serverId: %s)', serverId);
  await osCompute.startServer(serverId);
}

/**
 * @function stopServer
 * @description Stops the given server.
 * @param {string} serverId ID of server to stop
 * @return {string} ID of stopped server
 * @throws {HttpError} if OpenStack error occurs
 *   409 (conflict) if server status != 'ACTIVE' OR 'ERROR' (?)
 * @public
 */
async function stopServer(serverId) {
  logger.debug('(serverId: %s)', serverId);
  await osCompute.stopServer(serverId);
}

/**
 * @function updateServerMetadata
 * @description Update the metadata for the given server.
 * @param {string} serverId ID of server to update
 * @param {string} key metadata key
 * @param {string} value metadata value
 * @return none
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
async function updateServerMetadata(serverId, key, value) {
  logger.debug('(serverId: %s, key: %s, value: %s)', serverId, key, value);
  return osCompute.updateServerMetadata(serverId, key, value);
}

/**
 * module export(s)
 */

module.exports = {
  createServer,
  deleteServer,
  getFlavor,
  getFlavors,
  getImage,
  getImages,
  getServerMetadata,
  getServer,
  getServers,
  getServerStatus,
  health,
  initialize,
  isOwner,
  rebootServer,
  revokeToken,
  startServer,
  stopServer,
  updateServerMetadata
};
