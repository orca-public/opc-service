/**
 * Extended Error
 * @module XError
 */
/**
 * Copyright 2018 Virginia Polytechnic Institute and State University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * global(s) and required module(s)
 */

const path = require('path');
const util = require('util');

/**
 * @class XError
 * @classdesc Extended Error class. Allows for message formatting and
 *   also appends the error location to the message.
 *
 *   throw new XError('error retrieving data (code %d)',errorCode);
 *     message: 'error retrieving data (code 45) (someModule.js:45:16)'
 *
 * @extends Error
 */
class XError extends Error {
  constructor(message, ...args) {
    // can take multiple arguments for formatting like printf
    if (args.length) {
      // eslint-disable-next-line no-param-reassign
      message = util.format(message, args);
    }

    super(message);
    this.name = XError.name;

    // add error location to end of the message (module:line:offset)
    const tmp = this.stack.split('\n')[1].trim();
    if (tmp.startsWith('at')) {
      const errLocation = tmp.split('(')[1].slice(0, -1);
      this.location = path.basename(errLocation);
    }
  }
}

module.exports = XError;
