/**
 * OpenStack Image v2 API
 * @module openstackImage.v2
 *
 * Copyright 2019 Virginia Polytechnic Institute and State University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// -------------------------------------------------------------------
//   required module(s)
// -------------------------------------------------------------------

const httpStatus = require('http-status-codes');
const logger = require('simlog').getLogger();
const requestPromise = require('request-promise-native');

const XError = require('../utils/XError');

// -------------------------------------------------------------------
//   global constant(s)
// -------------------------------------------------------------------

const ENDPOINT_VERSION = 'v2';

const ERR_RETRIEVING_IMAGE = 'error retrieving image';
const ERR_RETRIEVING_IMAGE_DETAILS = 'error retrieving image details';

const HEADER_AUTH_TOKEN = 'x-auth-token';

const PATH_IMAGE = `/${ENDPOINT_VERSION}/images/:imageId`;
const PATH_IMAGES = `/${ENDPOINT_VERSION}/images?status=:status`;

// -------------------------------------------------------------------
//   module helper function(s)
// -------------------------------------------------------------------

/**
 * @function getBaseOptions
 * @description Returns the base request-promise options object.
 * @param {string} method HTTP method to include in options
 * @param {string} uri URI to include in options
 * @returns {object} base request-promise options object
 * @private
 */
function getBaseOptions(method, uri, token) {
  return {
    uri,
    method,
    headers: { [HEADER_AUTH_TOKEN]: token },
    json: true,
    simple: false,
    resolveWithFullResponse: true
  };
}

/**
 * @function normalizeImage
 * @description Normalizes the given image data.
 * @param {object} image image data to normalize
 * @return {object} normalized image data
 * @private
 */
function normalizeImage(image) {
  return {
    createdAt: image.created_at ? new Date(image.created_at) : undefined,
    diskFormat: image.disk_format,
    id: image.id,
    minDisk: image.min_disk ? parseInt(image.min_disk, 10) : 0,
    minRam: image.min_ram ? parseInt(image.min_ram, 10) : 0,
    name: image.name,
    self: PATH_IMAGE.replace(':imageId', image.id),
    size: image.size ? parseInt(image.size, 10) : 0,
    status: image.status,
    tags: image.tags,
    updatedAt: image.updated_at ? new Date(image.updated_at) : undefined,
    visibility: image.visibility
  };
}

// -------------------------------------------------------------------
//   class definition
// -------------------------------------------------------------------

/**
 * @class Image
 * @description Class handles OpenStack image calls.
 * @param {string} baseUrl base URL for the REST endpoint(s)
 */
function Image(baseUrl) {
  this.url = {
    image: `${baseUrl}${PATH_IMAGE}`,
    images: `${baseUrl}${PATH_IMAGES}`
  };
}

/**
 * @function getImage
 * @description Returns image details for the given image ID.
 * @param {string} token OpenStack auth token
 * @param {string} imageId ID of image to retrieve details for
 * @returns {object} image details
 * @throws {HttpError} if error(s) occur
 * @public
 */
Image.prototype.getImage = async function getImage(token, imageId) {
  const here = this.getImage.name;
  logger.debug('<%s> {token: *%s, imageId: %s}', here, token.substring(token.length - 4), imageId);

  // setup request
  const endpoint = this.url.image.replace(':imageId', imageId);
  const options = getBaseOptions('GET', endpoint, token);

  if (logger.isLevelTrace()) {
    const tmp = JSON.parse(JSON.stringify(options));
    tmp.headers[HEADER_AUTH_TOKEN] = `*${token.substring(token.length - 4)}`;
    logger.trace('<%s> {options: %o}', here, tmp);
  }

  // fetch image info
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.OK) {
    const error = new XError(ERR_RETRIEVING_IMAGE_DETAILS);
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }

  // normalize image info for return
  const normalizedImage = normalizeImage(response.body);
  logger.trace('<%s> {image: %o}', here, normalizedImage);

  return normalizedImage;
};

/**
 * @function getImages
 * @description Returns images.
 * @param {string} token OpenStack auth token
 * @param {string} imageStatus (optional) return images with the
 *   given status; by default, 'active' images are returned; values:
 *   active, deactivated, deleted, importing, killed, pending_delete,
 *   queued, saving, uploaded
 * @returns {array} available images
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
Image.prototype.getImages = async function getImages(token, imageStatus) {
  const here = this.getImages.name;
  const status = imageStatus || 'active';
  logger.debug('<%s> {token: %s, status: %s}', here, token.substring(token.length - 4), status);

  // setup request
  const endpoint = this.url.images.replace(':status', status);
  const options = getBaseOptions('GET', endpoint, token);

  if (logger.isLevelTrace()) {
    const tmp = JSON.parse(JSON.stringify(options));
    tmp.headers[HEADER_AUTH_TOKEN] = `*${token.substring(token.length - 4)}`;
    logger.trace('<%s> {options: %o}', here, tmp);
  }

  // fetch image info
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.OK) {
    const error = new XError(ERR_RETRIEVING_IMAGE);
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }

  // normalize image info for return
  const normalizedImages = [];
  for (let i = 0; i < response.body.images.length; i += 1) {
    normalizedImages.push(normalizeImage(response.body.images[i]));
  }

  logger.trace('<%s> {images: %o}', here, normalizedImages);

  return normalizedImages;
};

// -------------------------------------------------------------------
//   module export(s)
// -------------------------------------------------------------------

module.exports = {
  getInstance: baseUrl => new Image(baseUrl)
};
