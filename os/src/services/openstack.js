/**
 * <description>
 * @module <moduleName>
 *
 * Copyright 2019 Virginia Polytechnic Institute and State University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// -------------------------------------------------------------------
//   required module(s)
// -------------------------------------------------------------------

const osCompute = require('./openstackCompute.v2.1');
const osIdentity = require('./openstackIdentity.v3');
const osImage = require('./openstackImage.v2');
const osNetwork = require('./openstackNetwork.v2.0');

// -------------------------------------------------------------------
//   global constant(s)
// -------------------------------------------------------------------

// -------------------------------------------------------------------
//   module helper function(s)
// -------------------------------------------------------------------

// -------------------------------------------------------------------
//   class definition
// -------------------------------------------------------------------

/**
 * @class Openstack
 * @descripiton Main class for interacting with Openstack.
 * @param {object} osConfig class configuration
 */
function OpenStack(osConfig) {
  this.project = JSON.parse(JSON.stringify(osConfig.project));
  this.user = JSON.parse(JSON.stringify(osConfig.user));

  this.compute = osCompute.getInstance(osConfig.url.compute);
  this.identity = osIdentity.getInstance(osConfig.url.identity);
  this.image = osImage.getInstance(osConfig.url.image);
  this.network = osNetwork.getInstance(osConfig.url.network);

  this.auth = { token: 'need to authenticate first' };
}

/**
 * @function authenticate
 * @description Authenticate against OpenStack.
 * @returns none
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.authenticate = async function authenticate() {
  this.auth = await this.identity.authenticate(this.user, this.project);
};

/**
 * @function createServer
 * @description Creates a server instance in OpenStack.
 * @param {object} args arguments for creating the server
 *   {     serverName: <name to associate with new server>,
 *            imageId: <ID of image to create instance from>,
 *           flavorId: <instance flavor (RAM size, disk size, etc.)>,
 *          networkId: <ID of network instance will connect to>,
 *        keyPairName: <SSH keypair to allow access (optional)>,
 *           metadata: <object with key/value pairs (optional)>,
 *     securityGroups: <array of security access groups (optional)> }
 * @returns {string} ID of the newly created server instance
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.createServer = async function createServer(args) {
  return this.compute.createServer(this.auth.token, args);
};

/**
 * @function deleteServer
 * @description Deletes the server associated with the given server ID.
 * @param {string} serverId ID of server to delete
 * @returns none
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.deleteServer = async function deleteServer(serverId) {
  this.compute.deleteServer(this.auth.token, serverId);
};

/**
 * @function getFlavor
 * @description Get the flavor details for the given flavor ID.
 * @param {string} flavorId ID of flavor to query for
 * @returns {object} flavor details
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.getFlavor = async function getFlavor(flavorId) {
  return this.compute.getFlavor(this.auth.token, flavorId);
};

/**
 * @function getFlavors
 * @description Get the flavors available to use when creating a server.
 * @returns {array} flavor details
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.getFlavors = async function getFlavors() {
  return this.compute.getFlavors(this.auth.token);
};

/**
 * @function getImage
 * @description Get the image details for the given image ID.
 * @param {string} imageId ID of image to query for
 * @returns {object} image details
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.getImage = async function getImage(imageId) {
  return this.image.getImage(this.auth.token, imageId);
};

/**
 * @function getImages
 * @description Get the images available to use when creating a server.
 * @returns {array} image details
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.getImages = async function getImages(status) {
  return this.image.getImages(this.auth.token, status);
};

/**
 * @function getMetadata
 * @description Gets the server metadata for the given server ID.
 * @param {string} serverId ID of server to delete
 * @returns {object} server metadata
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.getMetadata = async function getMetadata(serverId) {
  return this.compute.getMetadata(this.auth.token, serverId);
};

/**
 * @function getServer
 * @description Get the server details for the given server ID.
 * @param {string} serverID ID of server to query for
 * @returns {object} server details
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.getServer = async function getServer(serverId) {
  return this.compute.getServer(this.auth.token, serverId);
};

/**
 * @function getServers
 * @description Get the details for all available servers.
 * @returns {array} available servers
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.getServers = async function getServers() {
  return this.compute.getServers(this.auth.token);
};

/**
 * @function rebootServer
 * @description Reboot the server with the given server ID.
 * @param {string} serverId ID of server to reboot
 * @returns none
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.rebootServer = async function rebootServer(serverId) {
  this.compute.rebootServer(this.auth.token, serverId);
};

/**
 * @function revokeToken
 * @description Revokes the current auth token.
 * @returns none
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.revokeToken = async function revokeToken() {
  await this.identity.revokeToken(this.auth.token);
};

/**
 * @function startServer
 * @description Start the server with the given server ID.
 * @param {string} serverId ID of server to start
 * @returns none
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.startServer = async function startServer(serverId) {
  this.compute.startServer(this.auth.token, serverId);
};

/**
 * @function stopServer
 * @description Stop the server with the given server ID.
 * @param {string} serverId ID of server to stop
 * @returns none
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.stopServer = async function stopServer(serverId) {
  this.compute.stopServer(this.auth.token, serverId);
};

/**
 * @function updateMetadata
 * @description Updates the server metadata with the given server ID.
 * @param {string} serverId ID of server
 * @param {string} key metadata key
 * @param {*} value metadata value
 * @returns none
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.updateMetadata = async function updateMetadata(serverId, key, value) {
  this.compute.updateServerMetadata(this.auth.token, serverId, key, value);
};

/**
 * @function validateToken
 * @description Validates the current authentication token.
 * @returns none
 * @throws {XError} if error(s) occured
 * @public
 */
OpenStack.prototype.validateToken = async function validateToken() {
  await this.identity.validateToken(this.auth.token);
};

OpenStack.prototype.getFloatingIps = async function getFloatingIps(floatingIp) {
  return this.network.getFloatingIps(this.auth.token, floatingIp);
};

OpenStack.prototype.getNetwork = async function getNetwork(networkName) {
  return this.network.getNetwork(this.auth.token, networkName);
};

OpenStack.prototype.getNetworks = async function getNetworks() {
  return this.network.getNetworks(this.auth.token);
};

OpenStack.prototype.getPorts = async function getPorts(deviceId) {
  return this.network.getPorts(this.auth.token, deviceId);
};

//  this.createFloatingIp = () => network.createFloatingIp(config.auth.project.id, externalNetworkId);
//  this.deleteFloatingIp = floatingIp => network.deleteFloatingIp(floatingIp);
//  this.getFloatingIps = () => network.getFloatingIps();
//  this.getNetwork = networkName => network.getNetwork(networkName);

// -------------------------------------------------------------------
//   module export(s)
// -------------------------------------------------------------------

module.exports = {
  getInstance: config => new OpenStack(config)
};
