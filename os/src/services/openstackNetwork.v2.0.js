/**
 * OpenStack Network API
 * @module openStackNetwork
 *
 * Copyright 2019 Virginia Polytechnic Institute and State University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// -------------------------------------------------------------------
//   required module(s)
// -------------------------------------------------------------------

const httpStatus = require('http-status-codes');
const logger = require('simlog').getLogger();
const requestPromise = require('request-promise-native');

const XError = require('../utils/XError');

// -------------------------------------------------------------------
//   global constant(s)
// -------------------------------------------------------------------

const ENDPOINT_VERSION = 'v2.0';

const PATH_DELETE_FLOATING_IP = `/${ENDPOINT_VERSION}/floatingips/:floatingIp`;
const PATH_FLOATING_IP = `/${ENDPOINT_VERSION}/floatingips/:floatingIpId`;
const PATH_FLOATING_IPS = `/${ENDPOINT_VERSION}/floatingips?status=:status`;
const PATH_NETWORK = `/${ENDPOINT_VERSION}/networks/:networkId`;
const PATH_NETWORKS = `/${ENDPOINT_VERSION}/networks`;
const PATH_PORT = `/${ENDPOINT_VERSION}/ports/:portId`;
const PATH_PORTS = `/${ENDPOINT_VERSION}/ports?device_id=:deviceId`;

const HEADER_AUTH_TOKEN = 'x-auth-token';

// -------------------------------------------------------------------
//   module helper function(s)
// -------------------------------------------------------------------

/**
 * @function getBaseOptions
 * @description Returns the base request-promise options object.
 * @param {string} method HTTP method to include in options
 * @param {string} uri URI to include in options
 * @returns {object} base request-promise options object
 * @private
 */
function getBaseOptions(method, uri, token) {
  return {
    uri,
    method,
    headers: { [HEADER_AUTH_TOKEN]: token },
    json: true,
    simple: false,
    resolveWithFullResponse: true
  };
}

/**
 * @function ifTraceLogOptions
 * @description Log given request-promise options if trace logging enabled.
 * @param {string} location name of the fuction doing the logging
 * @param {object} options request-promise options object
 * @param {string} token if given, the OpenStack auth token being used
 * @returns none
 * @private
 */
function ifTraceLogOptions(location, options, token) {
  if (logger.trace()) {
    const tmp = JSON.parse(JSON.stringify(options));

    // hide auth token
    if (tmp.headers[HEADER_AUTH_TOKEN]) {
      tmp.headers[HEADER_AUTH_TOKEN] = `*${token.substring(token.length - 4)}`;
    }

    logger.trace('<%s> {options: %s}', location, tmp);
  }
}

/**
 * @function normalizeFloatingIp
 * @description Normalizes the given floating IP data.
 * @param {object} floatingIp floating IP data to normalize
 * @return {object} normalized floating IP data
 * @private
 */
function normalizeFloatingIp(floatingIp) {
  return {
    createdAt: floatingIp.created_at ? new Date(floatingIp.created_at) : undefined,
    description: floatingIp.description,
    dnsName: floatingIp.dns_name,
    fixedIpAddress: floatingIp.fixed_ip_address,
    floatingIpAddress: floatingIp.floating_ip_address,
    floatingNetworkId: floatingIp.floating_network_id,
    id: floatingIp.id,
    portId: floatingIp.port_id,
    projectId: floatingIp.project_id,
    self: PATH_FLOATING_IP.replace(':floatingIpId', floatingIp.id),
    status: floatingIp.status,
    tenantId: floatingIp.tenant_id,
    updatedAt: floatingIp.updated_at ? new Date(floatingIp.updated_at) : undefined
  };
}

/**
 * @function normalizeNetwork
 * @description Normalizes the given network data.
 * @param {object} network network data to normalize
 * @return {object} normalized network data
 * @private
 */
function normalizeNetwork(network) {
  return {
    adminState: network.admin_state_up,
    availabilityZones: network.availability_zones,
    createdAt: network.created_at ? new Date(network.created_at) : undefined,
    description: network.description,
    dnsDomain: network.dns_domain,
    id: network.id,
    mtu: network.mtu,
    name: network.name,
    self: PATH_NETWORK.replace(':networkId', network.id),
    status: network.status,
    subnets: network.subnets,
    tags: network.tags,
    tenantId: network.tenant_id,
    updatedAt: network.updated_at ? new Date(network.updated_at) : undefined
  };
}

/**
 * @function normalizePort
 * @description Normalizes the given port data.
 * @param {object} port port data to normalize
 * @return {object} normalized port data
 * @private
 */
function normalizePort(port) {
  return {
    adminStateUp: port.admin_state_up,
    createdAt: port.created_at ? new Date(port.created_at) : undefined,
    deviceId: port.device_id,
    deviceOwner: port.device_owner,
    description: port.description,
    dnsAssignment: port.dns_assignment,
    dnsName: port.dns_name,
    fixedIps: port.fixed_ips,
    id: port.id,
    macAddress: port.mac_address,
    networkId: port.network_id,
    portSecurityEnabled: port.port_security_enabled,
    projectId: port.project_id,
    securityGroups: port.security_groups,
    self: PATH_PORT.replace(':portId', port.id),
    status: port.status,
    tenantId: port.tenant_id,
    updatedAt: port.updated_at ? new Date(port.updated_at) : undefined
  };
}

// -------------------------------------------------------------------
//   class definition
// -------------------------------------------------------------------

/**
 * @class Network
 * @description Class handles OpenStack network calls.
 * @param {object} osConfig
 */
function Network(baseUrl) {
  this.url = {
    createFloatingIp: `${baseUrl}${PATH_FLOATING_IPS}`,
    deleteFloatingIp: `${baseUrl}${PATH_DELETE_FLOATING_IP}`,
    floatingIps: `${baseUrl}${PATH_FLOATING_IPS}`,
    network: `${baseUrl}${PATH_NETWORK}`,
    networks: `${baseUrl}${PATH_NETWORKS}`,
    ports: `${baseUrl}${PATH_PORTS}`
  };
}

/**
 * @function createFloatingIp
 * @description Create a floating IP.
 * @param {string} token openstack auth token
 * @param {string} projectId project/tenant ID
 * @param {string} externalNetworkId ID of external network for the IP
 * @returns {object} floating IP information
 * @throws {XError} if OpenStack error occurs
 * @public
 */
Network.prototype.createFloatingIp = async function createFloatingIp(
  token,
  projectId,
  externalNetworkId
) {
  const here = this.createFloatingIp.name;
  logger.debug(
    '<%s> {token: *%s, projectId: %s, externalNetworkId: %s}',
    here,
    token.substring(token.length - 4),
    projectId,
    externalNetworkId
  );

  // setup request
  const options = getBaseOptions('POST', this.url.createFloatingIp, token);
  options.body = {
    floatingip: { project_id: projectId, floating_network_id: externalNetworkId }
  };

  ifTraceLogOptions(here, options, token);

  // create floating IP
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.CREATED) {
    const error = new XError('error creating floating IP');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }

  logger.trace('<%s> {floatingIp: %s}', here, response.body.floatingip);

  return response.body.floatingip;
};

/**
 * @function deleteFloatingIp
 * @description Delete the given floating IP.
 * @param {string} token openstack auth token
 * @param {string} floatingIp floating IP to delete
 * @returns none
 * @throws {XError} if OpenStack error occurs
 * @public
 */
Network.prototype.deleteFloatingIp = async function deleteFloatingIp(token, floatingIp) {
  const here = this.deleteFloatingIp.name;
  logger.debug(
    '<%s> {token: *%s, floatingIp: %s}',
    here,
    token.substring(token.length - 4),
    floatingIp
  );
  let id = null;

  // if 'floatingIp' is an IP address, we need to get the ID for it; if
  //   lookup fails, assume the 'floatingIp' is the ID
  try {
    const floatingIps = await this.getFloatingIps(token, floatingIp);
    [{ id }] = floatingIps;
  } catch (error) {
    id = floatingIp;
  }

  // setup request
  const endpoint = this.url.deleteFloatingIp.replace(':floatingIp', id);
  const options = getBaseOptions('DELETE', endpoint, token);

  ifTraceLogOptions(here, options, token);

  // delete floating IP
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.CREATED) {
    const error = new XError('error deleting floating IP');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }
};

/**
 * @function getFloatingIps
 * @description Returns the list of created floating IPs.
 * @param {string} token openstack auth token
 * @param {string} floatingIp floating IP address to get (optional)
 * @returns {array} array of floating IPs
 * @throws {XError} if OpenStack error occurs
 * @public
 */
Network.prototype.getFloatingIps = async function getFloatingIps(token, status) {
  const here = this.getFloatingIps.name;
  logger.debug('<%s> {token: *%s, status: %s}', here, token.substring(token.length - 4), status);

  // setup request
  let endpoint = null;
  if (status) {
    endpoint = this.url.floatingIps.replace(':status', status.toUpperCase());
  } else {
    const ndx = this.url.floatingIps.indexOf('?');
    if (ndx !== -1) {
      endpoint = this.url.floatingIps.substring(0, ndx);
    }
  }
  const options = getBaseOptions('GET', endpoint, token);

  ifTraceLogOptions(here, options, token);

  // get floating IPs
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.OK) {
    const error = new XError('error retrieving floating IP');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }

  const normalizedFloatingIps = [];
  for (let i = 0; i < response.body.floatingips.length; i += 1) {
    normalizedFloatingIps.push(normalizeFloatingIp(response.body.floatingips[i]));
  }
  logger.trace('<%s> {floatingIps: %O}', here, normalizedFloatingIps);

  return normalizedFloatingIps;
};

/**
 * @function getNetwork
 * @description Returns the details for the given network.
 * @param {string} token openstack auth token
 * @param  {string} networkName name of network to query
 * @returns {object} network details
 * @throws {XError} if OpenStack error occurs
 * @public
 */
Network.prototype.getNetwork = async function getNetwork(token, networkId) {
  const here = this.getNetwork.name;
  logger.debug(
    '<%s> {token: *%s, networkId: %s}',
    here,
    token.substring(token.length - 4),
    networkId
  );

  // setup request
  const endpoint = this.url.network.replace(':networkId', networkId);
  const options = getBaseOptions('GET', endpoint, token);

  ifTraceLogOptions(here, options, token);

  // get networks
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.OK) {
    const error = new XError('error retrieving network details');
    error.http = { status: response.statusCode, response: response.body.network };
    throw error;
  }

  const normalizedNetwork = normalizeNetwork(response.body.network);
  logger.trace('<%s> {network: %O}', here, normalizeNetwork);

  return normalizedNetwork;
};

/**
 * @function getNetworks
 * @description Returns the list of networks.
 * @param {string} token openstack auth token
 * @returns {array} array of networks
 * @throws {XError} if OpenStack error occurs
 * @public
 */
Network.prototype.getNetworks = async function getNetworks(token) {
  const here = this.getNetworks.name;
  logger.debug('<%s> {token: *%s}', here, token.substring(token.length - 4));

  // setup request
  const options = getBaseOptions('GET', this.url.networks, token);

  ifTraceLogOptions(here, options, token);

  // get networks
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.OK) {
    const error = new XError('error retrieving networks');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }

  const normalizedNetworks = [];
  for (let i = 0; i < response.body.networks.length; i += 1) {
    normalizedNetworks.push(normalizeNetwork(response.body.networks[i]));
  }

  logger.trace('<%s> {networks: %O}', here, normalizedNetworks);

  return normalizedNetworks;
};

/**
 * @function getPorts
 * @description Returns the list of ports for the given device ID.
 * @param {string} token openstack auth token
 * @param {string} deviceId ID of device to query ports for
 * @returns {array} array of port(s)
 * @throws {XError} if OpenStack error occurs
 * @public
 */
Network.prototype.getPorts = async function getPorts(token, deviceId) {
  const here = this.getPorts.name;
  logger.debug(
    '<%s> {token: *%s, deviceId: %s}',
    here,
    token.substring(token.length - 4),
    deviceId
  );

  // setup request
  const options = getBaseOptions('GET', this.url.ports.replace(':deviceId', deviceId), token);

  ifTraceLogOptions(here, options, token);

  // get networks
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.OK) {
    const error = new XError('error retrieving ports');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }

  const normalizedPorts = [];
  for (let i = 0; i < response.body.ports.length; i += 1) {
    normalizedPorts.push(normalizePort(response.body.ports[i]));
  }

  logger.trace('<%s> {networks: %O}', here, normalizedPorts);

  return normalizedPorts;
};

// --------------------------------------------------------------
//   module export(s)
// --------------------------------------------------------------

module.exports = {
  getInstance: baseUrl => new Network(baseUrl)
};
