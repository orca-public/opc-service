/**
 * OpenStack Compute API
 * @module openStackCompute.v2.1
 *
 * Copyright 2019 Virginia Polytechnic Institute and State University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// -------------------------------------------------------------------
//   required module(s)
// -------------------------------------------------------------------

const httpStatus = require('http-status-codes');
const logger = require('simlog').getLogger();
const requestPromise = require('request-promise-native');

const XError = require('../utils/XError');

// -------------------------------------------------------------------
//   global constant(s)
// -------------------------------------------------------------------

const ENDPOINT_VERSION = 'v2.1';

const PATH_CREATE_SERVER = `/${ENDPOINT_VERSION}/servers`;
const PATH_DELETE_SERVER = `/${ENDPOINT_VERSION}/servers/:serverId`;
const PATH_FLAVOR = `/${ENDPOINT_VERSION}/flavors/:flavorId`;
const PATH_FLAVORS = `/${ENDPOINT_VERSION}/flavors/detail`;
const PATH_METADATA = `/${ENDPOINT_VERSION}/servers/:serverId/metadata`;
const PATH_SERVER = `/${ENDPOINT_VERSION}/servers/:serverId`;
const PATH_SERVERS = `/${ENDPOINT_VERSION}/servers/detail`;
const PATH_SERVER_ACTION = `/${ENDPOINT_VERSION}/servers/:serverId/action`;
const PATH_UPDATE_METADATA = `/${ENDPOINT_VERSION}/servers/:serverId/metadata/:key`;

const HEADER_AUTH_TOKEN = 'x-auth-token';

// -------------------------------------------------------------------
//   module helper function(s)
// -------------------------------------------------------------------

/**
 * @function getBaseOptions
 * @description Returns the base request-promise options object.
 * @param {string} method HTTP method to include in options
 * @param {string} uri URI to include in options
 * @param {string} token OpenStack authentication token
 * @returns {object} base request-promise options object
 * @private
 */
function getBaseOptions(method, uri, token) {
  return {
    uri,
    method,
    headers: { [HEADER_AUTH_TOKEN]: token },
    json: true,
    simple: false,
    resolveWithFullResponse: true
  };
}

/**
 * @function ifTraceLogOptions
 * @description Log given request-promise options if trace logging enabled.
 * @param {string} location name of the fuction doing the logging
 * @param {object} options request-promise options object
 * @param {string} token if given, the OpenStack auth token being used
 * @returns none
 * @private
 */
function ifTraceLogOptions(location, options, token) {
  if (logger.isLevelTrace()) {
    const tmp = JSON.parse(JSON.stringify(options));

    // hide auth token
    if (tmp.headers[HEADER_AUTH_TOKEN]) {
      tmp.headers[HEADER_AUTH_TOKEN] = `*${token.substring(token.length - 4)}`;
    }

    logger.trace('<%s> {options: %s}', location, tmp);
  }
}

/**
 * @function normalizeFlavor
 * @description Normalizes the given flavor data.
 * @param {object} flavor flavor data to normalize
 * @param {object} config configuration data
 * @return {object} normalized flavor data
 * @private
 */
function normalizeFlavor(flavor) {
  return {
    diskSize: parseInt(flavor.disk, 10),
    id: flavor.id,
    name: flavor.name,
    public: flavor['os-flavor-access:is_public'],
    ram: parseInt(flavor.ram, 10),
    self: PATH_FLAVOR.replace(':flavorId', flavor.id),
    swap: flavor.swap ? parseInt(flavor.swap, 10) : 0,
    vcpus: parseInt(flavor.vcpus, 10)
  };
}

/**
 * @function normalizeServer
 * @description Normalizes the given server data.
 * @param {object} server server data to normalize
 * @param {object} flavor flavor data to include with server info
 * @param {object} addresses address data to include with server info
 * @param {object} config configuration data
 * @return {object} normalized server data
 * @private
 */
function normalizeServer(server, flavor) {
  const normalizedServer = {
    addresses: server.addresses,
    id: server.id,
    createdAt: server.created ? new Date(server.created) : undefined,
    fault: server.fault,
    flavor,
    hostId: server.hostId,
    launchedAt: server['OS-SRV-USG:launched_at']
      ? new Date(server['OS-SRV-USG:launched_at'])
      : undefined,
    metadata: server.metadata,
    name: server.name,
    securityGroups: server.security_groups,
    self: PATH_SERVER.replace(':serverId', server.id),
    status: server.status.toLowerCase(),
    tenantId: server.tenant_id,
    userId: server.user_id
  };

  // look for the floating IP and ASSUME it is the only one
  if (server.addresses) {
    let found = false;
    const networks = Object.keys(server.addresses);
    for (let i = 0; i < networks.length; i += 1) {
      if (!found) {
        const entry = server.addresses[networks[i]];
        for (let j = 0; j < entry.length; j += 1) {
          if (entry[j]['OS-EXT-IPS:type'] === 'floating') {
            normalizedServer.floatingIp = entry[j].addr;
            found = true;
            break;
          }
        }
      }
    }
  }

  return normalizedServer;
}

// -------------------------------------------------------------------
//   class definition
// -------------------------------------------------------------------

/**
 * @class Compute
 * @description Class handles OpenStack compute calls.
 * @param {object} osConfig
 */
function Compute(baseUrl) {
  this.url = {
    createServer: `${baseUrl}${PATH_CREATE_SERVER}`,
    deleteServer: `${baseUrl}${PATH_DELETE_SERVER}`,
    flavor: `${baseUrl}${PATH_FLAVOR}`,
    flavors: `${baseUrl}${PATH_FLAVORS}`,
    metadata: `${baseUrl}${PATH_METADATA}`,
    server: `${baseUrl}${PATH_SERVER}`,
    serverAction: `${baseUrl}${PATH_SERVER_ACTION}`,
    servers: `${baseUrl}${PATH_SERVERS}`,
    updateMetadata: `${baseUrl}${PATH_UPDATE_METADATA}`
  };
}

/**
 * @function createServer
 * @description Create a server instance.
 * @param {string} token openstack auth token
 * @param {object} args needed arguments object
 *   {     serverName: <name to associate with new server>,
 *            imageId: <ID of image to create instance from>,
 *           flavorId: <instance flavor (RAM size, disk size, etc.)>,
 *          networkId: <ID of network instance will connect to>,
 *        keyPairName: <SSH keypair to allow access (optional)>,
 *           metadata: <object with key/value pairs (optional)>,
 *     securityGroups: <array of security access groups (optional)> }
 * @returns {string} ID of the newly created server instance
 * @throws {XError} if OpenStack error occurs
 * @public
 */
Compute.prototype.createServer = async function createServer(token, args) {
  const here = this.createServer.name;
  logger.debug('<%s> {token: *%s, args: %o}', here, token.substring(token.length - 4), args);

  // setup request
  const options = getBaseOptions('POST', this.url.createServers, token);
  options.body = {
    server: {
      imageRef: args.imageId,
      flavorRef: args.flavorId,
      key_name: args.keyPairName,
      metadata: args.metadata,
      name: args.serverName,
      networks: [{ uuid: args.networkId }],
      security_groups: []
    }
  };

  if (args.securityGroups) {
    args.securityGroups.forEach(name => {
      options.body.server.security_groups.push({ name });
    });
  }

  ifTraceLogOptions(here, options, token);

  // create instance
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.ACCEPTED) {
    const error = new XError('error creating server');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }

  logger.trace('<%s> {serverId: %s}', here, response.body.server.id);

  return response.body.server.id;
};

/**
 * @function deleteServer
 * @description Deletes the server with the given ID.
 * @param {string} token openstack auth token
 * @param {string} serverId ID of server to delete
 * @returns none
 * @throws {XError} if OpenStack error occurs
 * @public
 */
Compute.prototype.deleteServer = async function deleteServer(token, serverId) {
  const here = this.deleteServer.name;
  logger.debug(
    '<%s> {token: *%s, serverId: %s}',
    here,
    token.substring(token.length - 4),
    serverId
  );

  // setup request
  const endpoint = this.url.delete.replace(':serverId', serverId);
  const options = getBaseOptions('DELETE', endpoint, token);

  ifTraceLogOptions(here, options, token);

  // delete the server
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.NO_CONTENT) {
    const error = new XError('error deleting server');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }
};

/**
 * @function getFlavor
 * @description Return flavor information for the given flavor ID.
 * @param {string} token openstack auth token
 * @param  {number} flavorId ID of flavor to retrieve info for
 * @returns {object} flavor information
 * @throws {XError} if OpenStack error occurs
 * @public
 */
Compute.prototype.getFlavor = async function getFlavor(token, flavorId) {
  const here = this.getFlavor.name;
  logger.debug(
    '<%s> {token: *%s, flavorId: %s}',
    here,
    token.substring(token.length - 4),
    flavorId
  );

  // setup request
  const endpoint = this.url.flavor.replace(':flavorId', flavorId);
  const options = getBaseOptions('GET', endpoint, token);

  ifTraceLogOptions(here, options, token);

  // fetch flavor
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.OK) {
    const error = new XError('error retrieving flavor details');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }

  // normalize flavor for return to caller
  const flavor = normalizeFlavor(response.body.flavor);
  logger.trace('<%s> {flavor: %o}', here, flavor);

  return flavor;
};

/**
 * @function getFlavors
 * @description Returns an array available flavors.
 * @param {string} token openstack auth token
 * @returns {array} projects user has access to
 * @throws {XError} if OpenStack error occurs
 * @public
 */
Compute.prototype.getFlavors = async function getFlavors(token) {
  const here = this.getFlavors.name;
  logger.debug('<%s> {token: *%s}', here, token.substring(token.length - 4));

  // setup request
  const options = getBaseOptions('GET', this.url.flavors, token);

  ifTraceLogOptions(here, options, token);

  // fetch flavors
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.OK) {
    const error = new XError('error retrieving flavors');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }

  // normalize flavors for return to caller
  const flavors = [];
  for (let i = 0; i < response.body.flavors.length; i += 1) {
    flavors.push(normalizeFlavor(response.body.flavors[i]));
  }

  logger.trace('<%s> {flavors: %o}', here, flavors);

  return flavors;
};

/**
 * @function getMetadata
 * @description Returns the metadata for the given server.
 * @param {string} token openstack auth token
 * @param {string} serverId ID of server to retrieve metadata for
 * @returns {object} server metadata if found, null if not found
 * @throws {XError} if OpenStack error occurs
 * @public
 */
Compute.prototype.getMetadata = async function getMetadata(token, serverId) {
  const here = this.getMetadata.name;
  logger.debug(
    '<%s> {token: *%s, serverId: %s}',
    here,
    token.substring(token.length - 4),
    serverId
  );

  // setup request
  const endpoint = this.url.metadata.replace(':serverId', serverId);
  const options = getBaseOptions('GET', endpoint, token);

  ifTraceLogOptions(here, options, token);

  // retrieve metadata
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.OK) {
    const error = new XError('error retrieving metadata');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }

  logger.trace('<%s> {response: %o}', here, response.body.metadata);

  return response.body.metadata;
};

/**
 * @function getServer
 * @description Return information for a specific server.
 * @param {string} token openstack auth token
 * @param {string} serverId ID of server to retrieve details for
 * @returns {array} server information if found, null if not found
 * @throws {XError} if error occurs
 * @public
 */
Compute.prototype.getServer = async function getServer(token, serverId) {
  const here = this.getServer.name;
  logger.debug(
    '<%s> {token: *%s, serverId: %s}',
    here,
    token.substring(token.length - 4),
    serverId
  );

  // setup request
  const endpoint = this.url.server.replace(':serverId', serverId);
  const options = getBaseOptions('GET', endpoint, token);

  if (logger.isLevelTrace()) {
    const tmp = JSON.parse(JSON.stringify(options));
    tmp.headers[HEADER_AUTH_TOKEN] = `*${token.substring(token.length - 4)}`;
    logger.trace('<%s> {options: %o}', here, tmp);
  }

  // fetch the server info
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.OK) {
    const error = new XError('error retrieving server details');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }

  // fetch flavor info for the server
  const flavor = await this.getFlavor(token, response.body.server.flavor.id);

  // normalize server info for return to caller
  const server = normalizeServer(response.body.server, flavor);
  logger.trace('<%s> {server: %o}', here, server);

  return server;
};

/**
 * @function getServers
 * @description Returns an array of servers.
 * @param {string} token openstack auth token
 * @returns {array} servers for the given owner; all servers if
 *   owner not given
 * @throws {XError} if OpenStack error occurs
 * @public
 */
Compute.prototype.getServers = async function getServers(token) {
  const here = this.getServers.name;
  logger.debug('<%s> {token: *%s}', here, token.substring(token.length - 4));

  // setup request
  const options = getBaseOptions('GET', this.url.servers, token);

  ifTraceLogOptions(here, options, token);

  // fetch servers
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.OK) {
    const error = new XError('error retrieving servers');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }

  // get flavor info for each server
  const results = [];
  for (let i = 0; i < response.body.servers.length; i += 1) {
    results.push(this.getFlavor(token, response.body.servers[i].flavor.id));
  }

  const values = await Promise.all(results);

  // normalize server info for return to caller
  const servers = [];
  for (let i = 0; i < values.length; i += 1) {
    servers.push(normalizeServer(response.body.servers[i], values[i]));
  }

  logger.trace('<%s> {servers: %o}', here, servers);

  return servers;
};

/**
 * @function rebootServer
 * @description Reboot the server with the given ID.
 * @param {string} token openstack auth token
 * @param {string} serverId ID of server to reboot
 * @param {string} type reboot type (soft, hard)
 * @returns none
 * @throws {XError} if OpenStack error occurs
 * @public
 */
Compute.prototype.rebootServer = async function rebootServer(token, serverId, rebootType) {
  const here = this.rebootServer.name;
  const type = rebootType ? rebootType.toUpperCase() : 'SOFT';

  logger.debug(
    '<%s> {token: *%s, serverId: %s, rebootType: %s}',
    here,
    token.substring(token.length - 4),
    serverId,
    type
  );

  // setup request
  const endpoint = this.url.serverAction.replace(':serverId', serverId);
  const options = getBaseOptions('POST', endpoint, token);
  options.body = { reboot: type };

  ifTraceLogOptions(here, options, token);

  // reboot server
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.ACCEPTED) {
    const error = new XError('error rebooting server');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }
};

/**
 * @function startServer
 * @description Starts the server with the given ID.
 * @param {string} token openstack auth token
 * @param {string} serverId ID of server to start
 * @returns none
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
Compute.prototype.startServer = async function startServer(token, serverId) {
  const here = this.startServer.name;
  logger.debug(
    '<%s> {token: *%s, serverId: %s}',
    here,
    token.substring(token.length - 4),
    serverId
  );

  // setup request
  const endpoint = this.url.serverAction.replace(':serverId', serverId);
  const options = getBaseOptions('POST', endpoint, token);
  options.body = { 'os-start': null };

  ifTraceLogOptions(here, options, token);

  // start server
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.ACCEPTED) {
    const error = new XError('error starting server');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }
};

/**
 * @function stopServer
 * @description Stops the server with the given ID.
 * @param {string} token openstack auth token
 * @param {string} serverId ID of server to start
 * @returns none
 * @throws {XError} if OpenStack error occurs
 * @public
 */
Compute.prototype.stopServer = async function stopServer(token, serverId) {
  const here = this.stopServer.name;
  logger.debug(
    '<%s> {token: *%s, serverId: %s}',
    here,
    token.substring(token.length - 4),
    serverId
  );

  // setup request
  const endpoint = this.url.serverAction.replace(':serverId', serverId);
  const options = getBaseOptions('POST', endpoint, token);
  options.body = { 'os-stop': null };

  ifTraceLogOptions(here, options, token);

  // start server
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.ACCEPTED) {
    const error = new XError('error stopping server');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }
};

/**
 * @function updateMetadata
 * @description Adds, updates,or deletes metadata for the given
 *   server. If the 'value' is not given, the metadata id deleted.
 * @param {string} token openstack auth token
 * @param {string} severId ID of server to update
 * @param {string} key metadata key
 * @param {string} value metadata value
 * @returns none
 * @throws {HttpError} if OpenStack error occurs
 * @public
 */
Compute.prototype.updaetServerMetadata = async function updateServerMetadata(
  token,
  serverId,
  key,
  value
) {
  const here = this.updateServerMetadata.name;
  logger.debug(
    '<%s> {token: *%s, serverId: %s, key: %s, value: %s}',
    here,
    token.substring(token.length - 4),
    serverId,
    key,
    value
  );

  // setup request
  const endpoint = this.url.updateMetadata.replace(':serverId', serverId).replace(':key', key);
  const options = getBaseOptions('PUT', endpoint, token);

  ifTraceLogOptions(here, options, token);

  let validResponse;

  // if value given, we are updating/adding; otherwise we are deleting
  if (typeof value === 'undefined') {
    options.method = 'DELETE';
    validResponse = httpStatus.NO_CONTENT;
  } else {
    options.body = { meta: { [key]: value } };
    validResponse = httpStatus.OK;
  }

  // update metadata
  const response = await requestPromise(options);
  if (response.statusCode !== validResponse) {
    const error = new XError('error updating metadata');
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }
};

// -------------------------------------------------------------------
//   module export(s)
// -------------------------------------------------------------------

module.exports = {
  getInstance: baseUrl => new Compute(baseUrl)
};
