/**
 * OpenStack Identity v3 API
 * @module openstackIdentity.v3
 *
 * Copyright 2019 Virginia Polytechnic Institute and State University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// -------------------------------------------------------------------
//   required module(s)
// -------------------------------------------------------------------

const httpStatus = require('http-status-codes');
const logger = require('simlog').getLogger();
const requestPromise = require('request-promise-native');

const XError = require('../utils/XError');

// -------------------------------------------------------------------
//   global constant(s)
// -------------------------------------------------------------------

const ENDPOINT_VERSION = 'v3';

const ERR_AUTHENTICATION = 'error authenticating';
const ERR_TOKEN_REVOCATION = 'error revoking token';
const ERR_TOKEN_VALIDATION = 'error validating token';

const HEADER_AUTH_TOKEN = 'x-auth-token';
const HEADER_SUBJECT_TOKEN = 'x-subject-token';

const PATH_AUTHENTICATE = `/${ENDPOINT_VERSION}/auth/tokens`;

// -------------------------------------------------------------------
//   module helper function(s)
// -------------------------------------------------------------------

/**
 * @function getBaseOptions
 * @description Returns the base request-promise options object.
 * @param {string} method HTTP method to include in options
 * @param {string} uri URI to include in options
 * @returns {object} base request-promise options object
 * @private
 */
function getBaseOptions(method, uri, token) {
  const options = {
    uri,
    method,
    headers: {},
    json: true,
    simple: false,
    resolveWithFullResponse: true
  };

  if (token) {
    options.headers[HEADER_AUTH_TOKEN] = token;
  }

  return options;
}

/**
 * @function ifTraceLogOptions
 * @description Log given request-promise options if trace logging enabled.
 * @param {string} location name of the fuction doing the logging
 * @param {object} options request-promise options object
 * @param {string} token if given, the OpenStack auth token being used
 * @returns none
 * @private
 */
function ifTraceLogOptions(location, options, token) {
  if (logger.trace()) {
    const tmp = JSON.parse(JSON.stringify(options));

    // hide auth token
    if (tmp.headers[HEADER_AUTH_TOKEN]) {
      tmp.headers[HEADER_AUTH_TOKEN] = `*${token.substring(token.length - 4)}`;
    }

    // hide subject token (revoke, validate)
    if (tmp.headers[HEADER_SUBJECT_TOKEN]) {
      tmp.headers[HEADER_SUBJECT_TOKEN] = tmp.headers[HEADER_AUTH_TOKEN];
    }

    // hide password (authentication)
    if (tmp.body && tmp.body.auth) {
      tmp.body.auth.identity.password.user.password = '****';
    }

    logger.trace('<%s> {options: %s}', location, tmp);
  }
}

// -------------------------------------------------------------------
//   class definition
// -------------------------------------------------------------------

/**
 * @class Identity
 * @description Class handles OpenStack identity calls.
 * @param {object} baseUrl
 */
function Identity(baseUrl) {
  this.url = {
    authenticate: `${baseUrl}${PATH_AUTHENTICATE}`
  };
}

/**
 * @function authenticate
 * @description Authenticates a user against OpenStack.
 * @param {object} user user information
 *   {   domain: <domain user belongs do>,
 *         name: <user name>,
 *     password: <user password> }
 * @param {object} project project information
 *   { domain: <project domain>,
 *       name: <project name> }
 * @returns {object} authorization info
 *   {     token: <token to use for subsequent calls>,
 *     expiresAt: <token expiration timestamp> }
 * @throws {XError} if error(s) occur
 * @public
 */
Identity.prototype.authenticate = async function authenticate(user, project) {
  const here = this.authenticate.name;

  if (logger.isLevelDebug()) {
    const tmp = JSON.parse(JSON.stringify(user));
    tmp.password = '****';
    logger.debug('<%s> {user: %O, project: %O}', here, tmp, project);
  }

  // setup request
  const options = getBaseOptions('POST', this.url.authenticate);
  options.body = {
    auth: {
      identity: {
        methods: ['password'],
        password: {
          user: { domain: { name: user.domain }, name: user.name, password: user.password }
        }
      },
      scope: {
        project: { domain: { name: project.domain }, name: project.name }
      }
    }
  };

  ifTraceLogOptions(here, options);

  // authenticate and receive auth token
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.CREATED) {
    const error = new XError(ERR_AUTHENTICATION);
    error.http = { status: response.statusCode, response: response.body };
    logger.error('<%s> {message: %s, response: %O}', here, error.message, error.http);
    throw error;
  }

  // get token and token expiration to return to caller
  const auth = {
    token: response.headers[HEADER_SUBJECT_TOKEN],
    expiresAt: new Date(response.body.token.expires_at)
  };

  if (logger.isLevelTrace()) {
    const tmp = JSON.parse(JSON.stringify(auth));
    tmp.token = `*${tmp.token.substring(tmp.token.length - 4)}`;
    logger.trace('<%s> {auth: %O}', here, tmp);
  }

  return auth;
};

/**
 * @function revokeToken
 * @description Revokes the given token.
 * @param {string} token token to be revoked
 * @param {boolean} throwError if true, exception thrown if
 *   revocation fails
 * @returns none
 * @throws none
 * @public
 */
Identity.prototype.revokeToken = async function revokeToken(token, throwError) {
  const here = this.revokeToken.name;
  const throwOnFailure = throwError || false;
  logger.debug(
    '<%s> {token: *%s, throwError: %s}',
    here,
    token.substring(token.length - 4),
    throwOnFailure
  );

  // setup request
  const options = getBaseOptions('DELETE', this.url.authenticate, token);
  options.headers[HEADER_SUBJECT_TOKEN] = token;

  ifTraceLogOptions(here, options, token);

  // request token revocation (note docs say it returns a 201)
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.NO_CONTENT) {
    const error = new XError(ERR_TOKEN_REVOCATION);
    error.http = { status: response.statusCode, response: response.body };
    if (throwOnFailure) {
      logger.error('<%s> {message: %s, response: %O}', here, error.message, error.http);
      throw error;
    } else {
      logger.warn('<%s> {message: %s, response: %O}', here, error.message, error.http);
    }
  }
};

/**
 * @function validateToken
 * @description Validates the given token.
 * @param {string} token token to validate
 * @return none
 * @throws {HttpError} if error(s) occur
 * @public
 */
Identity.prototype.validateToken = async function validateToken(token) {
  const here = this.validateToken.name;
  logger.debug('<%s> {token: *%s}', here, token.substring(token.length - 4));

  // setup request
  const options = getBaseOptions('HEAD', this.url.authenticate, token);
  options.headers[HEADER_SUBJECT_TOKEN] = token;

  ifTraceLogOptions(here, options, token);

  // request token validation
  const response = await requestPromise(options);
  if (response.statusCode !== httpStatus.OK) {
    const error = new XError(ERR_TOKEN_VALIDATION);
    error.http = { status: response.statusCode, response: response.body };
    throw error;
  }
};

// -------------------------------------------------------------------
//   module export(s)
// -------------------------------------------------------------------

module.exports = {
  getInstance: baseUrl => new Identity(baseUrl)
};
