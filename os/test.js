const util = require('util');

const logger = require('simlog').getLogger();
const openstack = require('./src/services/openstack');

const config = {
  url: {
    compute: 'https://api.isb-1.opc.vt.edu:13774',
    identity: 'https://api.isb-1.opc.vt.edu:13000',
    image: 'https://api.isb-1.opc.vt.edu:13292',
    network: 'https://api.isb-1.opc.vt.edu:13696'
  },
  externalNetworkId: '<Openstack network ID>',
  user: {
    domain: '<Openstack domain>',
    name: '<user>',
    password: '<password>'
  },
  project: {
    domain: '<Openstack project domain>',
    name: '<Openstack project name>'
  }
};

// *********************************************************

async function getFlavors(os) {
  console.log('Flavors:');

  const data = await os.getFlavors();
  const results = [];
  for (let i = 0; i < data.length; i += 1) {
    results.push(os.getFlavor(data[i].id));
  }

  const values = await Promise.all(results);

  for (let i = 0; i < values.length; i += 1) {
    console.log(' ', values[i].name, values[i].ram, values[i].diskSize);
  }
}

// *********************************************************

async function getFloatingIps(os) {
  console.log('Floating IPs:');

  const data = await os.getFloatingIps();
  for (let i = 0; i < data.length; i += 1) {
    console.log(' ', data[i].floatingIpAddress, data[i].fixedIpAddress, data[i].status);
  }
}

// *********************************************************

async function getImages(os) {
  console.log('Images:');

  const data = await os.getImages();
  const results = [];
  for (let i = 0; i < data.length; i += 1) {
    results.push(os.getImage(data[i].id));
  }

  const values = await Promise.all(results);

  for (let i = 0; i < values.length; i += 1) {
    console.log(' ', values[i].name, values[i].minDisk);
  }
}

// *********************************************************

async function getNetworks(os) {
  console.log('Networks:');

  const data = await os.getNetworks();
  const results = [];
  for (let i = 0; i < data.length; i += 1) {
    results.push(os.getNetwork(data[i].id));
  }

  const values = await Promise.all(results);

  for (let i = 0; i < values.length; i += 1) {
    console.log(' ', util.inspect(values[i],{ sorted: true, depth: null, showHidden: true }));
  }
}

// *********************************************************

async function getServers(os) {
  console.log('Servers:');

  const data = await os.getServers();
  const results = [];
  let ports = [];
  for (let i = 0; i < data.length; i += 1) {
    results.push(os.getServer(data[i].id));
    ports.push(os.getPorts(data[i].id));
  }

  const values = await Promise.all(results);
  ports = await Promise.all(ports);

  for (let i = 0; i < values.length; i += 1) {
    console.log(' ', util.inspect(ports[i], true, null));
  }
}

// ***********************************************************
//  main
// ***********************************************************

const os = openstack.getInstance(config);

os.authenticate()
  .then(async () => {
    await os.validateToken();
    await getFloatingIps(os);
    await getFlavors(os);
    await getImages(os);
    await getNetworks(os);
    await getServers(os);
    await os.revokeToken();
  })
  .catch(error => {
    logger.error(error);
  });
